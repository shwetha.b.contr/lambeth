﻿using Autofac;
using Autofac.Core;
using Autofac.Integration.WebApi;
using BancolombiaMDTTracking.Api.Controllers;
using BancolombiaMDTTracking.Core.Factories;
using BancolombiaMDTTracking.Core.Repository;
using BancolombiaMDTTracking.Core.Services;
using System;
using System.Reflection;
using System.Web.Http;

namespace BancolombiaMDTTracking.Api.App_Start
{
    public class AutofacConfig
    {

        public static IContainer Container;
        public static void Initialize(HttpConfiguration config)
        {
            Initialize(config, RegisterServices(new ContainerBuilder()));
        }

        public static void Initialize(HttpConfiguration config, IContainer container)
        {
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static IContainer RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // Repositories
            builder.RegisterType<DbFactory>().As<IDbFactory>().AsImplementedInterfaces();
            builder.RegisterType<UsersRepository>().As<IUsersRepository>().AsImplementedInterfaces();
            builder.RegisterType<UserTypeRepository>().As<IUserTypeRepository>().AsImplementedInterfaces();
            builder.RegisterType<CheckInRepository>().As<ICheckInRepository>().AsImplementedInterfaces();
            // Services
            builder.RegisterType<UsersService>().As<IUsersService>().AsImplementedInterfaces();
            builder.RegisterType<UserTypeService>().As<IUserTypeService>().AsImplementedInterfaces();
            builder.RegisterType<CheckinService>().As<ICheckinService>().AsImplementedInterfaces();
            builder.RegisterType<Image_SegmentService>().As<IImage_SegmentService>().AsImplementedInterfaces();
            Container = builder.Build();

            return Container;
        }

        public static T Resolve<T>()
        {
            if (Container == null)
            {
                throw new Exception("AutofacConfig hasn't been Initialize!");
            }

            return Container.Resolve<T>(new Parameter[0]);
        }
    }
}