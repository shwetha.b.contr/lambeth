﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using BancolombiaMDTTracking.Api.Filters;

namespace BancolombiaMDTTracking.Api.Controllers
{
    public class BaseApiController : ApiController
    {
        public Authentication UserLogged { get; set; }
        public IMLogger.Log log { get; set; }


        protected async Task<HttpResponseMessage> CreateHttpResponseAsync(HttpRequestMessage request, Func<Task<HttpResponseMessage>> function)
        {
            HttpResponseMessage response = null;

            try
            {
                var CurrentUser = HttpContext.Current.User;

                Thread.CurrentPrincipal = CurrentUser;

                if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated)
                {
                    if (Thread.CurrentPrincipal.Identity is Authentication)
                    {
                        Authentication basicAuthenticationIdentity = Thread.CurrentPrincipal.Identity as Authentication;
                        UserLogged = basicAuthenticationIdentity;
                        log = IMLogger.Log.GetInstance();
                        //socket = WebSocketService.GetInstance();
                    }
                }
                response = await function.Invoke();
            }
            catch (Exception ex)
            {
                response = request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }

            return response;
        }

    }
}