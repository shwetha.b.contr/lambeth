﻿using BancolombiaMDTTracking.Core.Entities.Dto;
using BancolombiaMDTTracking.Core.Services;
using LambethTracking.Core.Entities.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BancolombiaMDTTracking.Api.Controllers
{
    [RoutePrefix("api/Checkin")]
    public class CheckinController : BaseApiController
    {
        private readonly ICheckinService _checkinService;

        public CheckinController(ICheckinService checkinService)
        {
            _checkinService = checkinService;
        }

       
        [HttpGet]
        [Route("api/Checkin/GetBox/{boxCode}/{type}")]
        public async Task<HttpResponseMessage> GetBoxDetails(HttpRequestMessage request, string boxCode, string type)
        {
            return await CreateHttpResponseAsync(request, async () =>
            {
                HttpResponseMessage response = null;
                string message = String.Empty;
                try
                {
                    var checkInDtoObj = _checkinService.GetBoxDetails(boxCode,type);

                    if(checkInDtoObj != null)
                    {
                        response = request.CreateResponse(HttpStatusCode.OK, checkInDtoObj);
                    }
                    else
                    {
                        response = request.CreateResponse(HttpStatusCode.OK, checkInDtoObj);
                    }
                }
                catch (Exception ex)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest,
                    new
                    {
                        message = ex.Message
                    });

                }

                return await Task.FromResult(response);
            });
        }

        [HttpPost]
        [Route("api/Checkin/SaveBoxDetails/{CheckInDtoObj}")]
        public async Task<HttpResponseMessage> SaveBoxDetails(HttpRequestMessage request, CheckInDto CheckInDtoObj)
        {
            return await CreateHttpResponseAsync(request, async () =>
            {
                HttpResponseMessage response = null;
                string message = String.Empty;
                try
                {
                    message = _checkinService.SaveBoxDetails(CheckInDtoObj);
                    response = request.CreateResponse(HttpStatusCode.OK, message);
                }
                catch (Exception ex)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest,
                    new
                    {
                        message = "Box deatils are failed to insert/update."
                    });

                }

                return await Task.FromResult(response);
            });
        }

        [HttpGet]
        [Route("api/Checkin/GetDocTypes")]
        public async Task<HttpResponseMessage> GetDocTypes(HttpRequestMessage request)
        {
            return await CreateHttpResponseAsync(request, async () =>
            {
                HttpResponseMessage response = null;
                HashSet<string> docTypes = new HashSet<string>();
                string message = String.Empty;
                try
                {
                    docTypes = _checkinService.GetDocTypes();

                    if (docTypes != null)
                    {
                        response = request.CreateResponse(HttpStatusCode.OK, docTypes);
                    }
                    else
                    {
                        response = request.CreateResponse(HttpStatusCode.OK, docTypes);
                    }
                }
                catch (Exception ex)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest,
                    new
                    {
                        message = ex.Message
                    });

                }

                return await Task.FromResult(response);
            });
        }


        [HttpPost]
        [Route("api/Checkin/SaveTids/{boxId}/{tidDtoObj}")]
        public async Task<HttpResponseMessage> SaveTids(HttpRequestMessage request, string boxId, List<TID_Dto> tidDtoObj)
        {
            return await CreateHttpResponseAsync(request, async () =>
            {
                HttpResponseMessage response = null;
                string message = String.Empty;
                try
                {                    
                    message = _checkinService.SaveTids(boxId,tidDtoObj);
                    response = request.CreateResponse(HttpStatusCode.OK, message);
                }
                catch (Exception ex)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest,
                    new
                    {
                        message = "Tid deatils are failed to insert."
                    });

                }

                return await Task.FromResult(response);
            });
        }


        [HttpGet]
        [Route("api/Checkin/GetTidsList/{boxId}")]
        public async Task<HttpResponseMessage> GetTidsList(HttpRequestMessage request, string boxId)
        {
            return await CreateHttpResponseAsync(request, async () =>
            {
                HttpResponseMessage response = null;
                string message = String.Empty;
                try
                {
                    var item = _checkinService.GetTidsList(boxId);

                    response = request.CreateResponse(HttpStatusCode.OK, item);
                }
                catch (Exception ex)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest,
                    new
                    {
                        message = ex.Message
                    });
                }

                return await Task.FromResult(response);
            });
        }


        [Route("api/Checkin/SaveTidsList/{boxId}/{userName}/{skpbox}")]
        [HttpPost]        
        public async Task<HttpResponseMessage> SaveTidsList(HttpRequestMessage request, string boxId, string userName, string skpbox)
        {
            return await CreateHttpResponseAsync(request, async () =>
            {
                HttpResponseMessage response = null;
                string message = String.Empty;
                try
                {
                    message = _checkinService.SaveTidsList(boxId, userName, skpbox);
                    response = request.CreateResponse(HttpStatusCode.OK, message);
                }
                catch (Exception ex)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest,
                    new
                    {
                        message = "Tid deatils are failed to insert."
                    });

                }

                return await Task.FromResult(response);
            });
        }

        
        [HttpDelete]
        public async Task<HttpResponseMessage> DeleteTid(HttpRequestMessage request, long tid)
        {
            return await CreateHttpResponseAsync(request, async () =>
            {
                HttpResponseMessage response = null;
                string message = String.Empty;
                try
                {
                    message = _checkinService.DeleteTid(tid);
                    response = request.CreateResponse(HttpStatusCode.OK, message);
                }
                catch (Exception ex)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest,
                    new
                    {
                        message = "Tid deatils are failed to delete."
                    });

                }

                return await Task.FromResult(response);
            });
        }


        [HttpPost]
        [Route("api/Checkin/SaveHoldBoxDetails/{CheckInDtoObj}")]
        public async Task<HttpResponseMessage> SaveHoldBoxDetails(HttpRequestMessage request, CheckInDto CheckInDtoObj)
        {
            return await CreateHttpResponseAsync(request, async () =>
            {
                HttpResponseMessage response = null;
                string message = String.Empty;
                try
                {
                    message = _checkinService.SaveHoldBoxDetails(CheckInDtoObj);
                    response = request.CreateResponse(HttpStatusCode.OK, message);
                }
                catch (Exception ex)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest,
                    new
                    {
                        message = "Box deatils file is not created."
                    });

                }

                return await Task.FromResult(response);
            });
        }

        [HttpPost]
        [Route("api/Checkin/CheckItemCodeFromExceldata/{boxCode}")]
        public async Task<HttpResponseMessage> CheckItemCodeFromExcelData(HttpRequestMessage request, string boxCode)
        {
            return await CreateHttpResponseAsync(request, async () =>
            {
                HttpResponseMessage response = null;
                string message = String.Empty;
                try
                {
                    var checkInDtoObj = _checkinService.CheckItemCodeFromExcelData(boxCode);

                    if (checkInDtoObj != null)
                    {
                        response = request.CreateResponse(HttpStatusCode.OK, checkInDtoObj);
                    }
                    else
                    {
                        response = request.CreateResponse(HttpStatusCode.OK, checkInDtoObj);
                    }
                }
                catch (Exception ex)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest,
                    new
                    {
                        message = ex.Message
                    });

                }

                return await Task.FromResult(response);
            });
        }

        [HttpGet]
        [Route("api/Checkin/GetBoxIdList/{tid}")]
        public async Task<HttpResponseMessage> GetBoxIdList(HttpRequestMessage request, string tid)
        {
            return await CreateHttpResponseAsync(request, async () =>
            {
                HttpResponseMessage response = null;
                string message = String.Empty;
                try
                {
                    var item = _checkinService.GetBoxIdList(tid);

                    response = request.CreateResponse(HttpStatusCode.OK, item);
                }
                catch (Exception ex)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest,
                    new
                    {
                        message = ex.Message
                    });
                }

                return await Task.FromResult(response);
            });
        }

    }
}