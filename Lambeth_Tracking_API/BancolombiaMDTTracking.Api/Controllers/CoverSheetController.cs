﻿using BancolombiaMDTTracking.Api.Models;
using BancolombiaMDTTracking.Api.Utils;
using dbconnection;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Description;

namespace BancolombiaMDTTracking.Api.Controllers
{
    [RoutePrefix("api/CoverSheet")]
    public class CoverSheetController : BaseApiController
    {

        [Route("CreateCoverSheetByBatch")]
        [HttpPost]
        public async Task<HttpResponseMessage> CreateCoverSheetByBatch(HttpRequestMessage request, [FromBody] JObject data)
        {
            return await CreateHttpResponseAsync(request, async () =>
            {
                HttpResponseMessage response = null;
                string message = String.Empty;
                try
                {
                    log.setMessage("CoverSheet", $"CoverSheet Controller - CreateCoverSheetByBatch", UserLogged.UserID.ToString());
                    log.setMessage("CoverSheet", $"Generating CoverSheet", UserLogged.UserID.ToString());
                    var items = new List<CoverSheetBatch>();

                    items = data["data"].ToObject<List<CoverSheetBatch>>();

                    //items = items.OrderBy(i => i.BatchName).ToList();

                    CoverSheet coverSheet = new CoverSheet();
                    List<CoverSheetPageBatch> pages = new List<CoverSheetPageBatch>();
                    pages.Add(new CoverSheetPageBatch { Items = items });

                    //List<CoverSheetPageReport> reports = new List<CoverSheetPageReport>();
                    //reports.Add(new CoverSheetPageReport { Items = itemsReports });

                    coverSheet.CoverPagesInfoBatch = pages;
                    //coverSheet.ReportPagesInfo = reports;
                    coverSheet.BarcodeConfiguration = new BarcodeConfiguration();
                    coverSheet.PageDataConfiguration = new PageDataConfiguration();
                    coverSheet.BarcodeConfiguration.Type = "CODE 39 EXTENDED";
                    //coverSheet.PathIVImageLocation = HostingEnvironment.MapPath("~/App_Data/P_T.PNG");
                    if (coverSheet.LoadConfig("Batch"))
                    {
                        if (coverSheet.GenerateByBatches())
                        {
                            log.setMessage("CoverSheet", $"CoverSheet Generated", UserLogged.UserID.ToString());
                            response = request.CreateResponse(HttpStatusCode.OK, coverSheet.CoverSheetFile);
                        }
                        else
                        {
                            log.setMessage("CoverSheet", $"Can't Generate The Coversheets", UserLogged.UserID.ToString());
                            response = request.CreateResponse(HttpStatusCode.InternalServerError, "Can't Generate The Coversheets.");
                        }
                    }
                    else
                    {
                        log.setMessage("CoverSheet", $"Can't Load The coversheet configuration", UserLogged.UserID.ToString());
                        response = request.CreateResponse(HttpStatusCode.InternalServerError, "Can't Load The coversheet configuration.");
                    }



                }
                catch (Exception ex)
                {
                    log.setMessage("CoverSheet", $"Error: {ex.Message}", UserLogged.UserID.ToString());
                    response = request.CreateResponse(HttpStatusCode.InternalServerError,
                    new
                    {
                        message = ex.Message
                    });
                }

                return await Task.FromResult(response);
            });

        }


        [Route("CreateCoverSheetTID")]
        [HttpPost]
        public async Task<HttpResponseMessage> CreateCoverSheetTID(HttpRequestMessage request, [FromBody] JObject data)
        {
            return await CreateHttpResponseAsync(request, async () =>
            {
                HttpResponseMessage response = null;
                string message = String.Empty;
                try
                {
                    log.setMessage("CoverSheet", $"CoverSheet Controller - CreateCoverSheetTID", UserLogged.UserID.ToString());
                    log.setMessage("CoverSheet", $"Generating CoverSheet by TID", UserLogged.UserID.ToString());
                    var items = new List<CoversheetModel>();
                    var itemsReports = new List<DigitalizationReportModel>();

                    items = data["data"].ToObject<List<CoversheetModel>>();

                    //items = items.OrderBy(i => i.BatchName).ToList();

                    CoverSheet coverSheet = new CoverSheet();
                    List<CoverSheetPageTID> pages = new List<CoverSheetPageTID>();
                    //pages.Add(new CoverSheetPageTID { Items = items });


                    coverSheet.CoverPagesInfoTID = pages;
                    //coverSheet.ReportPagesInfo = reports;
                    coverSheet.BarcodeConfiguration = new BarcodeConfiguration();
                    coverSheet.PageDataConfiguration = new PageDataConfiguration();
                    //coverSheet.PathIVImageLocation = HostingEnvironment.MapPath("~/App_Data/P_T.PNG");
                    if (coverSheet.LoadConfig())
                    {
                        if (coverSheet.GenerateTID())
                        {
                            response = request.CreateResponse(HttpStatusCode.OK, coverSheet.CoverSheetFile);
                            log.setMessage("CoverSheet", $"CoverSheet generated", UserLogged.UserID.ToString());
                        }
                        else
                        {
                            log.setMessage("CoverSheet", $"Can't Generate The Coversheets.", UserLogged.UserID.ToString());
                            response = request.CreateResponse(HttpStatusCode.InternalServerError, "Can't Generate The Coversheets.");
                        }
                    }
                    else
                    {
                        log.setMessage("CoverSheet", $"Can't Load The coversheet configuration.", UserLogged.UserID.ToString());
                        response = request.CreateResponse(HttpStatusCode.InternalServerError, "Can't Load The coversheet configuration.");
                    }



                }
                catch (Exception ex)
                {
                    log.setMessage("CoverSheet", $"Error: {ex.Message}", UserLogged.UserID.ToString());
                    response = request.CreateResponse(HttpStatusCode.InternalServerError,
                    new
                    {
                        message = ex.Message
                    });
                }

                return await Task.FromResult(response);
            });

        }

        [Route("CreateCoverSheetByTID")]
        [HttpPost]
        public async Task<HttpResponseMessage> CreateCoverSheetByTID(HttpRequestMessage request, [FromBody] JObject data)
        {
            return await CreateHttpResponseAsync(request, async () =>
            {
                HttpResponseMessage response = null;
                string message = String.Empty;
                try
                {
                    log.setMessage("CoverSheet", $"CoverSheet Controller - CreateCoverSheetByTID", UserLogged.UserID.ToString());
                    log.setMessage("CoverSheet", $"Generating CoverSheet", UserLogged.UserID.ToString());
                    var items = new List<CoversheetModel>();
                    var itemsReports = new List<DigitalizationReportModel>();

                    items = data["data"].ToObject<List<CoversheetModel>>();

                    //items = items.OrderBy(i => i.BatchName).ToList();

                    CoverSheet coverSheet = new CoverSheet();
                    List<CoverSheetPageTID> pages = new List<CoverSheetPageTID>();
                    pages.Add(new CoverSheetPageTID { Items = items });




                    //List<CoverSheetPageReport> reports = new List<CoverSheetPageReport>();
                    //reports.Add(new CoverSheetPageReport { Items = itemsReports });

                    coverSheet.CoverPagesInfoTID = pages;
                    //coverSheet.ReportPagesInfo = reports;
                    coverSheet.BarcodeConfiguration = new BarcodeConfiguration();
                    coverSheet.PageDataConfiguration = new PageDataConfiguration();
                    //coverSheet.PathIVImageLocation = HostingEnvironment.MapPath("~/App_Data/P_T.PNG");

                    switch ("Patch_T")
                    {
                        case "Patch_I":
                            coverSheet.PathIVImageLocation = HostingEnvironment.MapPath("~/App_Data/Patch_I.PNG");
                            break;
                        case "Patch_II":
                            coverSheet.PathIVImageLocation = HostingEnvironment.MapPath("~/App_Data/Patch_II.PNG");
                            break;
                        case "Patch_III":
                            coverSheet.PathIVImageLocation = HostingEnvironment.MapPath("~/App_Data/Patch_III.PNG");
                            break;
                        case "Patch_IV":
                            coverSheet.PathIVImageLocation = HostingEnvironment.MapPath("~/App_Data/Patch_IV.PNG");
                            break;
                        case "Patch_VI":
                            coverSheet.PathIVImageLocation = HostingEnvironment.MapPath("~/App_Data/Patch_VI.PNG");
                            break;
                        case "Patch_T":
                            coverSheet.PathIVImageLocation = HostingEnvironment.MapPath("~/App_Data/Patch_T.PNG");
                            break;
                        default:
                            coverSheet.PathIVImageLocation = null;
                            break;

                    }
                    if (coverSheet.LoadConfig())
                    {
                        if (coverSheet.GenerateByTID())
                        {
                            log.setMessage("CoverSheet", $"CoverSheet generated", UserLogged.UserID.ToString());
                            response = request.CreateResponse(HttpStatusCode.OK, coverSheet.CoverSheetFile);
                        }
                        else
                        {
                            log.setMessage("CoverSheet", $"Can't Generate The Coversheets", UserLogged.UserID.ToString());
                            response = request.CreateResponse(HttpStatusCode.InternalServerError, "Can't Generate The Coversheets.");
                        }
                    }
                    else
                    {
                        log.setMessage("CoverSheet", $"Can't Load The coversheet configuration.", UserLogged.UserID.ToString());
                        response = request.CreateResponse(HttpStatusCode.InternalServerError, "Can't Load The coversheet configuration.");
                    }



                }
                catch (Exception ex)
                {
                    log.setMessage("CoverSheet", $"Error: {ex.Message}", UserLogged.UserID.ToString());
                    response = request.CreateResponse(HttpStatusCode.InternalServerError,
                    new
                    {
                        message = ex.Message
                    });
                }

                return await Task.FromResult(response);
            });

        }

    }
}