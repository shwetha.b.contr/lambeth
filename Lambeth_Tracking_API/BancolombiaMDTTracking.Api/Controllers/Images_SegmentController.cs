﻿using BancolombiaMDTTracking.Core.Config;
using BancolombiaMDTTracking.Core.Dtos;
using BancolombiaMDTTracking.Core.Entities.Dto;
using BancolombiaMDTTracking.Core.Services;
using dbconnection;
using GdPicture14;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

using TiffDLL90;

namespace BancolombiaMDTTracking.Api.Controllers
{
    [RoutePrefix("api/Images_Segment")]
    public class Images_SegmentController: BaseApiController
    {
        private readonly IImage_SegmentService _imageSegmentService;

        public Images_SegmentController(IImage_SegmentService imageSegmentService)
        {
            _imageSegmentService = imageSegmentService;
            

        }


        [HttpPost]
        [Route("upload/{batchid}/{segment}/{e2e}")]
        public async Task<HttpResponseMessage> UploadImageSegment(HttpRequestMessage request, long batchid, string segment, long e2e)
        {
            return await CreateHttpResponseAsync(request, async () =>
            {
                HttpResponseMessage response = null;
                string message = String.Empty;
                HashSet<string> hashFiles = new HashSet<string>();

                try
                {
                    string root = ReadConfig.PathImages
                        .Replace("[%BATCH_ID%]", batchid.ToString())
                        .Replace("[%SEGMENT%]", segment);

                    if (!Debugger.IsAttached)
                    {
                        root = HttpContext.Current.Server.MapPath(Path.Combine(HttpContext.Current.Request.ApplicationPath, root));
                    }
                    if (!Directory.Exists(root))
                    {
                        Directory.CreateDirectory(root);
                    }

                    var provider = new MultipartFormDataStreamProvider(root);
                    await request.Content.ReadAsMultipartAsync(provider);

                    foreach (var file in provider.FileData)
                    {
                        var buffer = File.ReadAllBytes(file.LocalFileName);
                        string _originalfilename = file.Headers.ContentDisposition.FileName.Replace("\"", "");

                        string fileExt = ".tiff";//Path.GetExtension(_originalfilename);

                        string _filename = GenerateName(batchid, segment);
                        string filenameThumb = _filename + "_s";

                        

                        string _fullfilename = root + _filename + fileExt;



                        string fileExtentionThumb = ".jpg"; // Path.GetExtension(_fullfilename);
                        string rootThumbnail = ReadConfig.PhysicalPathThumbnails
                            .Replace("[%BATCH_ID%]", batchid.ToString())
                            .Replace("[%SEGMENT%]", segment);

                        string _fullfilenameThumb = rootThumbnail + filenameThumb + fileExtentionThumb;

                        if (!Directory.Exists(rootThumbnail))
                        {
                            Directory.CreateDirectory(rootThumbnail);
                        }

                        hashFiles.Add(_filename + fileExt + "," + filenameThumb + fileExtentionThumb);

                        //File.WriteAllBytes(_fullfilename, buffer);

                        //Save file as TIFF
                        using (GdPictureImaging gdpictureImaging = new GdPictureImaging())
                        {
                            //int imageID = gdpictureImaging.CreateGdPictureImageFromFile(_fullfilename);
                            int imageID = gdpictureImaging.CreateGdPictureImageFromByteArray(buffer);

                            gdpictureImaging.SaveAsTIFF(imageID, _fullfilename, TiffCompression.TiffCompressionLZW);

                            // Release used resources.
                            gdpictureImaging.ReleaseGdPictureImage(imageID);
                        }


                        //delete temp file generated
                        System.IO.File.Delete(file.LocalFileName);

                        //save image thumbnail

                        using (GdPictureImaging gdpictureImaging = new GdPictureImaging())
                        {
                            int imageID = gdpictureImaging.CreateGdPictureImageFromFile(_fullfilename);

                            // Create a thumbnail 
                            int width = 200;
                            double porc = width * 100 / gdpictureImaging.GetWidth(imageID);
                            int heigth = (int)porc * gdpictureImaging.GetHeight(imageID) / 100;

                            int thumbnailID = gdpictureImaging.CreateThumbnail(imageID, width, heigth);
                            gdpictureImaging.SaveAsJPEG(thumbnailID, _fullfilenameThumb, 8); // 0 best compression and worst quality

                            // Release used resources.
                            gdpictureImaging.ReleaseGdPictureImage(thumbnailID);
                            gdpictureImaging.ReleaseGdPictureImage(imageID);
                        }
                    }

                    foreach (var hfile in hashFiles)
                    {
                        var files = hfile.Split(',');
                        //Images_Segment images_segment = new Images_Segment
                        //{
                        //    Batch_Id = batchid,
                        //    Image_Name = files[0],
                        //    Image_Thumbnail = files[1],
                        //    Image_Status = "KRDY",
                        //    Segment = segment,
                        //    Creation_Date = DateTime.UtcNow,
                        //    Source = "IMPORT",
                        //    User_Name = UserLogged.UserName,
                        //    Order_Id = _imageSegmentService.GetNextOrderId(batchid, segment)
                        //};

                        //_imageSegmentService.InsertUpdateImage(images_segment, out message);

                        //e2e
                        //try { this._e2eService.E2E_Update_Package(e2e, 1); } catch (Exception e) { }

                        response = request.CreateResponse(HttpStatusCode.OK,
                    new
                    {
                        message = message
                    });
                    }
                }
                catch (Exception ex)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest,
                    new
                    {
                        message = ex.Message
                    });
                }

                return await Task.FromResult(response);
            });
        }

        [HttpGet]
        [Route("List/{BatchId}/{Segment}")]
        public async Task<HttpResponseMessage> GetImagesSegment(HttpRequestMessage request, long BatchId, string Segment)
        {
            return await CreateHttpResponseAsync(request, async () =>
            {
                HttpResponseMessage response = null;
                string message = String.Empty;
                try
                {
                    //var items = _imageSegmentService.GetImagesSegment_BeforePull(BatchId, Segment);
                    //response = request.CreateResponse(HttpStatusCode.OK, items);
                    response = request.CreateResponse(HttpStatusCode.OK, "");
                }
                catch (Exception ex)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest,
                    new
                    {
                        message = ex.Message
                    });
                }

                return await Task.FromResult(response);
            });
        }

        [HttpPost]
        [Route("Save")]
        public async Task<HttpResponseMessage> InsertImage_Segment(HttpRequestMessage request, [FromBody] JObject data)
        {
            return await CreateHttpResponseAsync(request, async () =>
            {
                HttpResponseMessage response = null;
                string message = String.Empty;
                string message_byimg = String.Empty;
                
                try
                {
                    //var image_segment = data["image_segment"].ToObject<Images_Segment>();
                    //image_segment.User_Name = UserLogged.UserName;

                    //bool success = _imageSegmentService.InsertUpdateImage(image_segment, out message);
                    //Images_SegmentDto images_segmentdto = null;
                    //if (success)
                    //{
                    //    images_segmentdto = _imageSegmentService.GetImagesSegment_BeforeScan(image_segment.Batch_Id, image_segment.Id, out message_byimg);
                    //    try {this._e2eService.E2E_Update_Package(data["e2e"].ToObject<int>(), 1); } catch (Exception e) { }
                    //}

                    response = request.CreateResponse(HttpStatusCode.OK,
                            new
                            {
                                message = message,
                                //images_segmentdto = images_segmentdto
                            });
                }
                catch (Exception ex)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest,
                    new
                    {
                        message = ex.Message
                    });
                }

                return await Task.FromResult(response);
            });
        }

        [HttpPost]
        [Route("Swap")]
        public async Task<HttpResponseMessage> Swap_Image_Segment(HttpRequestMessage request, [FromBody] JObject data)
        {
            return await CreateHttpResponseAsync(request, async () =>
            {
                HttpResponseMessage response = null;
                string message = String.Empty;
                try
                {
                    bool success = false;
                    //var image_segment_swap = data.ToObject<Image_Segment_Swap>();

                    //if (image_segment_swap.images_change.Count > 0)
                    //{
                    //    var imagesCurrent = _imageSegmentService.GetImagesSegment(image_segment_swap.images_change[0].Batch_Id, image_segment_swap.images_change[0].Segment);

                    //    for (int i = 0; i < imagesCurrent.Count; i++)
                    //    {
                    //        var img_seg = image_segment_swap.images_change.FirstOrDefault(x => x.Id == imagesCurrent[i].Id);
                    //        if(img_seg != null && img_seg.Order_Id != imagesCurrent[i].Order_Id)
                    //        {
                    //            success = _imageSegmentService.UpdateImage(img_seg, out message);
                    //        }
                    //    }
                    //}
                    
                    response = request.CreateResponse(HttpStatusCode.OK,
                        new
                        {
                            message = message
                        });
                }
                catch (Exception ex)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest,
                    new
                    {
                        message = ex.Message
                    });
                }

                return await Task.FromResult(response);
            });
        }

        [HttpPost]
        [Route("SavePhotos")]
        public async Task<HttpResponseMessage> InsertImage_Segment_Photos(HttpRequestMessage request, [FromBody] JObject data)
        {
            HttpResponseMessage response = null;
            string message = String.Empty;
            //string root = ReadConfig.PathImages;
            string root = "/LambethTrack/ImagePath/";
            string filePath = string.Empty;
            var imagePath = string.Empty;
            var image_segment_photos = data["image_segment_photos"].ToObject<Image_Segment_Photos>();
           


            return await CreateHttpResponseAsync(request, async () =>
            {
                
                try
                {
                    string virtualDirectory = HttpContext.Current.Server.MapPath(System.IO.Path.Combine(HttpContext.Current.Request.ApplicationPath, root));
                    string folderPath = string.Format(@"{0}\{1}", virtualDirectory, image_segment_photos._TID);

                    int photoCount = 1;
                    foreach (var photo in image_segment_photos.Photos)
                    {
                        imagePath = string.Format(@"{0}\{1}_{2}.tiff", folderPath, image_segment_photos._TID, photoCount);
                        photoCount++;
                        filePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, imagePath);

                        if (!System.IO.Directory.Exists(folderPath))
                        {
                            System.IO.Directory.CreateDirectory(folderPath);
                        }

                        SaveImageFromBase64String(image_segment_photos.Photos[0], filePath, 0, image_segment_photos._TID);
                    }



                    response = request.CreateResponse(HttpStatusCode.OK,
                            new
                            {
                                message = message
                            });
                }
                catch (Exception ex)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest,
                    new
                    {
                        message = ex.Message
                    });
                }

                return await Task.FromResult(response);
            });
        }

        


        private void SaveImageFromBase64String(string base64ImageString, string imgPath, int rotation, long tid)
        {
           
            try
            {
                // Remove the first secton to hav pure image object.
                var cleanerBase64 = base64ImageString.Replace("data:image/png;base64,", "");  //base64ImageString.Substring(22);
               
               
                byte[] imageBytes = Convert.FromBase64String(cleanerBase64);

                //Convert
                Image img;
                using (MemoryStream ms1 = new MemoryStream(imageBytes))
                {
                    img = Image.FromStream(ms1);
                    ms1.Dispose();
                }

                TiffDLL tiffObject = null;

                // Load TiffDLL interface (access to Tifftek32.dll)
                tiffObject = new TiffDLL();

                // Set Registration Code
                tiffObject._RegistrationCode = "GDCISBXH4400111300311066";

                int tiffObjectResult;

                // add TID in the TIFF file
                Image tidTextImage = DrawText(tid.ToString());
                Bitmap mergedBmp = new Bitmap(Math.Max(tidTextImage.Width, img.Width), tidTextImage.Height + img.Height);
                using (Graphics g = Graphics.FromImage(mergedBmp))
                {
                    g.Clear(Color.White);
                    g.DrawImage(img, 0, 0);
                    g.DrawImage(tidTextImage, 0, img.Height);
                }

                // Open source file (page to be added).
                //tiffObject.Sourcebitmap = new System.Drawing.Bitmap(img); //
                tiffObject.Sourcebitmap = new System.Drawing.Bitmap(mergedBmp); //this contains the TID
                tiffObject._OpenFile.Filename = "bitmap";

                tiffObject._SaveFile.Format = TiffDLL90.Format.Auto; //Auto

                // Save to Tiff 
                tiffObject._SaveFile.Filename = imgPath;

                // Execute Task (This method calls to functionality that is packaged in TiffDLL90 'Tifftek32.dll')
                tiffObjectResult = tiffObject.Run();

                // Evaluate results from object (TiffDLL90), If it is not '0' (zero) something is wrong
                if (tiffObjectResult != 0)
                {
                    // Get error code.
                    string ErrorCode = tiffObjectResult.ToString();

                    // Create an exception.
                    throw new Exception(string.Format("Cannot access to Image Info File, code: {0}", tiffObjectResult));
                }
            }
            catch (Exception err)
            {
                
            }
        }

        /// <summary>
        /// Creates an image containing the given text.
        /// NOTE: the image should be disposed after use.
        /// </summary>
        /// <param name="text">Text to draw</param>
        /// <param name="fontOptional">Font to use, defaults to Control.DefaultFont</param>
        /// <param name="textColorOptional">Text color, defaults to Black</param>
        /// <param name="backColorOptional">Background color, defaults to white</param>
        /// <param name="minSizeOptional">Minimum image size, defaults the size required to display the text</param>
        /// <returns>The image containing the text, which should be disposed after use</returns>
        public Image DrawText(string text, Font fontOptional = null, Color? textColorOptional = null, Color? backColorOptional = null, Size? minSizeOptional = null)
        {
            Font font = new Font("Arial", 14F);
            if (fontOptional != null)
                font = fontOptional;

            Color textColor = Color.Black;
            if (textColorOptional != null)
                textColor = (Color)textColorOptional;

            Color backColor = Color.White;
            if (backColorOptional != null)
                backColor = (Color)backColorOptional;

            Size minSize = Size.Empty;
            if (minSizeOptional != null)
                minSize = (Size)minSizeOptional;

            //first, create a dummy bitmap just to get a graphics object
            SizeF textSize;
            using (Image img = new Bitmap(1, 1))
            {
                using (Graphics drawing = Graphics.FromImage(img))
                {
                    //measure the string to see how big the image needs to be
                    textSize = drawing.MeasureString(text, font);
                    if (!minSize.IsEmpty)
                    {
                        textSize.Width = textSize.Width > minSize.Width ? textSize.Width : minSize.Width;
                        textSize.Height = textSize.Height > minSize.Height ? textSize.Height : minSize.Height;
                    }
                }
            }

            //create a new image of the right size
            Image retImg = new Bitmap((int)textSize.Width, (int)textSize.Height);
            using (var drawing = Graphics.FromImage(retImg))
            {
                //paint the background
                drawing.Clear(backColor);

                //create a brush for the text
                using (Brush textBrush = new SolidBrush(textColor))
                {
                    drawing.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                    drawing.DrawString(text, font, textBrush, 0, 0);
                    drawing.Save();
                }
            }
            return retImg;
        }

        
      

        public string GenerateName(long batchid, string segment)
        {
            string filename = batchid.ToString() + "_" +
                        segment + "_" +
                        DateTime.UtcNow.Year.ToString() +
                        DateTime.UtcNow.Month.ToString() +
                        DateTime.UtcNow.Day.ToString() +
                        DateTime.UtcNow.Hour.ToString() +
                        DateTime.UtcNow.Minute.ToString() +
                        DateTime.UtcNow.Second.ToString() +
                        DateTime.UtcNow.Millisecond.ToString();
            return filename;
        }
  
    }
}