﻿using dbconnection;
using BancolombiaMDTTracking.Core.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using IMLogger;

namespace BancolombiaMDTTracking.Api.Controllers
{
    [RoutePrefix("api/User")]
    public class UserController : BaseApiController
    {
        private readonly IUsersService _usersService;

        public UserController(IUsersService usersService)
        {

            _usersService = usersService;

        }

        [HttpPost]
        [Route("Login")]
        public async Task<HttpResponseMessage> Login(HttpRequestMessage request)
        {

            return await CreateHttpResponseAsync(request, async () =>
            {
                HttpResponseMessage response = null;
                string message = String.Empty;
                try
                {
                    if (UserLogged != null)
                    {
                        log.setMessage("User", $"User Controller - Login", UserLogged.UserID.ToString());
                        var op = _usersService.VerifyUserLogin(UserLogged.UserName, UserLogged.Password);
                        User = Thread.CurrentPrincipal;
                        response = request.CreateResponse(HttpStatusCode.OK, new { user = op });
                        log.setMessage("User", "User Login by " + UserLogged.UserName, UserLogged.UserID.ToString());
                        
                    }
                    else
                    {

                        message = "Invalid username or password, please try again.";
                        response = request.CreateResponse(HttpStatusCode.NotFound, new { message = message, Sucess = false });
                    }

                }
                catch (Exception ex)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest,
                    new
                    {
                        Message = ex.Message
                    });
                }

                return await Task.FromResult(response);
            });
        }

        [HttpGet]
        [Route("List")]
        public async Task<HttpResponseMessage> GetUsers(HttpRequestMessage request)
        {
            return await CreateHttpResponseAsync(request, async () =>
            {
                HttpResponseMessage response = null;
                string message = String.Empty;
                try
                {
                    log.setMessage("User", $"User Controller - List", UserLogged.UserID.ToString());
                    var item = _usersService.GetUsers();
                    response = request.CreateResponse(HttpStatusCode.OK, item);
                    log.setMessage("Users", "Get users", UserLogged.UserID.ToString());
                }
                catch (Exception ex)
                {
                    log.setMessage("User", "Error: " + ex.Message, UserLogged.UserID.ToString());
                    response = request.CreateResponse(HttpStatusCode.BadRequest,
                    new
                    {
                        message = ex.Message
                    });
                   
                }

                return await Task.FromResult(response);
            });
        }

        [Route("GetUser/{id:int=0}/")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetUser(HttpRequestMessage request, int id)
        {
            return await CreateHttpResponseAsync(request, async () =>
            {
                HttpResponseMessage response = null;
                string message = String.Empty;
                try
                {
                    log.setMessage("User", "User Controller - GetUser", UserLogged.UserID.ToString());
                    var user = _usersService.GetUser(id);
                    //var site_id = _usersService.GetSiteIDUser(id);
                    response = request.CreateResponse(HttpStatusCode.OK, new { user });
                    log.setMessage("User", "Get user by ID " + id, UserLogged.UserID.ToString());
                }
                catch (Exception ex)
                {
                    log.setMessage("User", "Error: " + ex.Message, UserLogged.UserID.ToString());
                    response = request.CreateResponse(HttpStatusCode.BadRequest,
                    new
                    {
                        exception = ex.Message
                    });
                }

                return await Task.FromResult(response);
            });
        }

        [HttpPost]
        [Route("Save")]
        public async Task<HttpResponseMessage> Save(HttpRequestMessage request, [FromBody] JObject data)
        {
            return await CreateHttpResponseAsync(request, async () =>
            {
                HttpResponseMessage response = null;
                string message = String.Empty;
                try
                {
                   // log.setMessage("User", "User Controller - Save", UserLogged.UserID.ToString());
                    var model = data["model"].ToObject<User>();
                   // log.setMessage("User", $"Inserting or updating User {model.UserName}", UserLogged.UserID.ToString());
                    var result = _usersService.InsertUpdateUser(model, out message);
                    if (result)
                    {
                      //  log.setMessage("User", $"User saved", UserLogged.UserID.ToString());
                        response = request.CreateResponse(HttpStatusCode.OK,
                        new
                        {
                            message = message
                        });
                        
                    }
                    else
                    {
                      //  log.setMessage("User", $"User no saved : {message}", UserLogged.UserID.ToString());
                        response = request.CreateResponse(HttpStatusCode.BadRequest,
                        new
                        {
                            message = message
                        });
                    }

                }
                catch (Exception ex)
                {
                    log.setMessage("User", $"Error : {ex.Message}", UserLogged.UserID.ToString());
                    response = request.CreateResponse(HttpStatusCode.BadRequest,
                    new
                    {
                        message = ex.Message
                    });
                }

                return await Task.FromResult(response);
            });
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<HttpResponseMessage> Delete(HttpRequestMessage request, int id)
        {
            return await CreateHttpResponseAsync(request, async () =>
            {
                HttpResponseMessage response = null;
                string message = String.Empty;
                try
                {
                    log.setMessage("User", $"User Controller - Delete", UserLogged.UserID.ToString());
                    log.setMessage("User", $"Deleting user {id}", UserLogged.UserID.ToString());
                    var result = _usersService.DeleteUser(id, out message);
                    if (result)
                    {
                        log.setMessage("User", $"User deleted", UserLogged.UserID.ToString());
                        response = request.CreateResponse(HttpStatusCode.OK,
                        new
                        {
                            message = message
                        });
                    }
                    else
                    {
                        log.setMessage("User", $"User no deleted : {message}", UserLogged.UserID.ToString());
                        response = request.CreateResponse(HttpStatusCode.BadRequest,
                        new
                        {
                            message = message
                        });
                    }

                }
                catch (Exception ex)
                {
                    log.setMessage("User", $"Error : {ex.Message}", UserLogged.UserID.ToString());
                    response = request.CreateResponse(HttpStatusCode.BadRequest,
                    new
                    {
                        error = "ERROR",
                        message = ex.Message
                    });
                }

                return await Task.FromResult(response);
            });
        }

    }
}