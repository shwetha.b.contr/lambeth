﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BancolombiaMDTTracking.Api.Models
{
    public class CoversheetFOPEP
    {
        public long TIDID { get; set; }
        public string Precinto { get; set; }
        public string PrecintoTula { get; set; }
        public string BatchName { get; set; }
        public string Codeffice { get; set; }
        public string NameOffice { get; set; }
        public string Sucursal { get; set; }
        public string Country { get; set; }
        public DateTime ManifestDate { get; set; }
        public long PIDID { get; set; }
        public DateTime TulaDate { get; set; }
        public string fopep { get; set; }
    }
}