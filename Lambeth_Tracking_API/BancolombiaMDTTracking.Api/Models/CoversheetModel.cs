﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BancolombiaMDTTracking.Api.Models
{
    public class CoversheetModel
    {
        public long TIDID { get; set; }
        public string SKPBoXNumber { get; set; }
        public string ServiceArea { get; set; }
        public string  SubServiceArea { get; set; }
        public string FinanceCode { get; set; }
        public string Precinto { get; set; }
        public string PrecintoTula { get; set; }
        public string BatchName { get; set; }
        public string Codeffice { get; set; }
        public string NameOffice { get; set; }
        public string Sucursal { get; set; }
        public string Country { get; set; }
        public DateTime ManifestDate { get; set; }
        public long PIDID { get; set; }
        public DateTime TulaDate { get; set; }
    }
}