﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BancolombiaMDTTracking.Api.Models
{
    public class DigitalizationReportModel
    {
        public string Country { get; set; }
        public DateTime ManifestDate { get; set; }
        public string Tula { get; set; }
        public DateTime CheckinDate { get; set; }
        public string BatchNmae { get; set; }
    }
}