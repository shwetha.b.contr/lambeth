﻿using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.IO;
using BarcodeLib;
using iTextSharp.text.pdf;
using System.Drawing;
using dbconnection;
using BancolombiaMDTTracking.Api.Models;

namespace BancolombiaMDTTracking.Api.Utils
{
    public class CoverSheet
    {
        // Fields/Props
        private BarcodeLib.Barcode barcode = new BarcodeLib.Barcode();
        private List<String> _errors;
        public List<String> Errors
        {
            get { return _errors; }
            set { _errors = value; }
        }

        private MemoryStream _CoverSheetFile = null;
        public MemoryStream CoverSheetFile
        {
            get { return _CoverSheetFile; }
            private set { _CoverSheetFile = value; }
        }

        public string DocumentTypeText = string.Empty;
        public float DocumentTypeX = 290;
        public float DocumentTypeY = 750;
        public float DocumentTypeYB2 = 750;
        public float DocumentTypePercent = 40;

        public string SeparatorText = string.Empty;
        public float SeparatorX = 150;
        public float SeparatorY = 950;
        public float SeparatorPercent = 30;

        public string PathIVImageLocation = string.Empty;
        public string TempPath = string.Empty;
        private bool _configLoaded = false;
        private TYPE type;

        public PageDataConfiguration PageDataConfiguration;
        public BarcodeConfiguration BarcodeConfiguration;

        //public List<CoverSheetPage> CoverPagesInfo;
        public List<CoverSheetPageTID> CoverPagesInfoTID;
        public List<CoverSheetPageBatch> CoverPagesInfoBatch;
        public List<CoverSheetPageFOPEP> CoverPagesInfoFOPEP;
        public List<CoverSheetPageReport> ReportPagesInfo;
        public List<CoverSheetPageBoth> CoverSheetPageBoth;

        public List<CoverSheetInfo> Info { get; set; }
        //public List<CoverSheetInfoTID> InfoTID { get; set; }

        public List<System.Drawing.Image> BarcodeImages;

        // Constructor
        public CoverSheet()
        {
            _configLoaded = false;
            Errors = new List<string>();
            BarcodeImages = null;
            //CoverPagesInfo = null;
        }

        public bool LoadConfig(string coversheetType = null)
        {
            _configLoaded = false;
            Errors.Clear();
            try
            {
                // Validate Barcode Configuration
                if (BarcodeConfiguration != null && PageDataConfiguration != null)
                {
                    // Validate Barcode Data
                    barcode.Alignment = BarcodeConfiguration.AlignmentPosition;
                    barcode.Height = (BarcodeConfiguration.Height <= 0 ? 50 : BarcodeConfiguration.Height);
                    barcode.Width = (BarcodeConfiguration.Width <= 0 ? 250 : BarcodeConfiguration.Width);
                    barcode.IncludeLabel = BarcodeConfiguration.IncludeLabel;
                    type = TYPE.UNSPECIFIED;                                    
                    switch (BarcodeConfiguration.Type.ToUpper().Trim())
                    {
                        case "UPC-A": type = TYPE.UPCA; break;
                        case "UPC-E": type = TYPE.UPCE; break;
                        case "UPC 2 DIGIT EXT": type = TYPE.UPC_SUPPLEMENTAL_2DIGIT; break;
                        case "UPC 5 DIGIT EXT.": type = TYPE.UPC_SUPPLEMENTAL_5DIGIT; break;
                        case "EAN-13": type = TYPE.EAN13; break;
                        case "JAN-13": type = TYPE.JAN13; break;
                        case "EAN-8": type = TYPE.EAN8; break;
                        case "ITF-14": type = TYPE.ITF14; break;
                        case "CODABAR": type = TYPE.Codabar; break;
                        case "POSTNET": type = TYPE.PostNet; break;
                        case "BOOKLAND/ISBN": type = TYPE.BOOKLAND; break;
                        case "CODE 11": type = TYPE.CODE11; break;
                        case "CODE 39": type = TYPE.CODE39; break;
                        case "CODE 39 EXTENDED": type = TYPE.CODE39Extended; break;
                        case "CODE 93": type = TYPE.CODE93; break;
                        case "LOGMARS": type = TYPE.LOGMARS; break;
                        case "MSI": type = TYPE.MSI_Mod10; break;
                        case "INTERLEAVED 2 OF 5": type = TYPE.Interleaved2of5; break;
                        case "STANDARD 2 OF 5": type = TYPE.Standard2of5; break;
                        case "CODE 128": type = TYPE.CODE128; break;
                        case "CODE 128-A": type = TYPE.CODE128A; break;
                        case "CODE 128-B": type = TYPE.CODE128B; break;
                        case "CODE 128-C": type = TYPE.CODE128C; break;
                        case "TELEPEN": type = TYPE.TELEPEN; break;
                        case "FIM": type = TYPE.FIM; break;
                        default: Errors.Add("Please specify the encoding type."); break;
                    }


                    _configLoaded = true;
                }
                else
                {
                    Errors.Add("Barcode Configuration is not completed.");
                }
            }
            catch (Exception)
            {
                _configLoaded = false;
            }

            return _configLoaded;

        }

        public bool GenerateTID()
        {
            List<string> batchesName = new List<string>();
            bool retval = false;
            try
            {
                if (!_configLoaded)
                {
                    Errors.Add("Configuration not loaded.");
                }

                if (_configLoaded)
                {

                    BarcodeImages = new List<System.Drawing.Image>();
                    Document pdfDocument = null;
                    pdfDocument = new Document(PageSize.A4, 5f, 5f, 15f, 15f);

                    string tmpPdfFile = Path.Combine(TempPath, Guid.NewGuid().ToString()) + ".pdf";
                    MemoryStream pdfOutput = new MemoryStream();
                    PdfWriter writer = PdfWriter.GetInstance(pdfDocument, pdfOutput);
                    writer.CloseStream = false;
                    pdfDocument.Open();

                    foreach (var info in CoverPagesInfoTID)
                    {
                        foreach (var item in info.Items)
                        {
                            if (!batchesName.Contains(item.BatchName))
                            {

                                //create CoverSheet of Batch
                                batchesName.Add(item.BatchName);

                                pdfDocument.Add(new Paragraph(Environment.NewLine));
                                pdfDocument.Add(new Paragraph(Environment.NewLine));
                                string line = "Tula: " + item.PrecintoTula + Environment.NewLine;
                                var p = new Paragraph(line);
                                p.Alignment = Element.ALIGN_CENTER;
                                pdfDocument.Add(p);

                                line = "PID: " + item.PIDID.ToString() + Environment.NewLine;
                                var p2 = new Paragraph(line);
                                p2.Alignment = Element.ALIGN_CENTER;
                                pdfDocument.Add(p2);

                                line = "Fecha y Hora de Impresión: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + Environment.NewLine;
                                var p3 = new Paragraph(line);
                                p3.Alignment = Element.ALIGN_CENTER;
                                pdfDocument.Add(p3);

                                BarcodeLib.Barcode bBatch = new BarcodeLib.Barcode();
                                bBatch.BackColor = Color.White;
                                bBatch.ForeColor = Color.Black;
                                bBatch.IncludeLabel = true;
                                bBatch.LabelPosition = LabelPositions.BOTTOMCENTER;
                                System.Drawing.Font fontBatch = new System.Drawing.Font("verdana", 15f);
                                bBatch.LabelFont = fontBatch;
                                bBatch.Height = 200;
                                bBatch.Width = 700;

                                System.Drawing.Image dossier_bcBatch = bBatch.Encode(type, string.Format("{0}", item.BatchName.ToString()));
                                pdfDocument.Add(new Paragraph(Environment.NewLine));
                                pdfDocument.Add(new Paragraph(Environment.NewLine));
                                pdfDocument.Add(new Paragraph(Environment.NewLine));
                                pdfDocument.Add(new Paragraph(Environment.NewLine));
                                pdfDocument.Add(new Paragraph(Environment.NewLine));
                                iTextSharp.text.Image ibcBatch = iTextSharp.text.Image.GetInstance(dossier_bcBatch, BaseColor.WHITE);
                                ibcBatch.Alignment = Element.ALIGN_CENTER;
                                ibcBatch.ScalePercent(DocumentTypePercent);
                                pdfDocument.Add(ibcBatch);

                                pdfDocument.NewPage();

                            }


                            IDictionary<string, string> records = new Dictionary<string, string>();
                            records.Add("Tula", item.PrecintoTula.ToString());
                            records.Add("Precinto", item.Precinto.ToString());
                            records.Add("Batch Name", item.BatchName.ToString());
                            records.Add("Oficina", item.Codeffice == null ? "NO DATA" : item.Codeffice.ToString());
                            records.Add("Descripcion Oficina", item.NameOffice == null ? "NO DATA" : item.NameOffice.ToString());
                            records.Add("Sucursal", item.Sucursal == null ? "NO DATA" : item.Sucursal.ToString() == "BOGO" ? "BOGOTA" : item.Sucursal.ToString() == "CALI" ? "CALI" : item.Sucursal == "MEDE" ? "MEDELLIN" : "BARRANQUILLA");

                            BarcodeLib.Barcode b = new BarcodeLib.Barcode();
                            b.BackColor = Color.White;
                            b.ForeColor = Color.Black;
                            b.IncludeLabel = true;
                            b.LabelPosition = LabelPositions.BOTTOMCENTER;
                            System.Drawing.Font font = new System.Drawing.Font("verdana", 15f);
                            b.LabelFont = font;
                            b.Height = 150;
                            b.Width = 400;

                            System.Drawing.Image dossier_bc = b.Encode(type, string.Format("TID-{0}", item.TIDID.ToString()));

                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            pdfDocument.Add(new Paragraph(Environment.NewLine));

                            iTextSharp.text.Image ibc = iTextSharp.text.Image.GetInstance(dossier_bc, BaseColor.WHITE);
                            ibc.Alignment = Element.ALIGN_CENTER;
                            ibc.ScalePercent(DocumentTypePercent);
                            pdfDocument.Add(ibc);

                            for (int i = 0; i <= 10; i++)
                            {
                                pdfDocument.Add(new Paragraph(Environment.NewLine));
                            }


                            foreach (var record in records)
                            {
                                string line = record.Key + ": " + record.Value + Environment.NewLine;
                                var p = new Paragraph(line);
                                p.Alignment = Element.ALIGN_CENTER;
                                pdfDocument.Add(p);
                            }
                            // Add ne page.
                            pdfDocument.NewPage();
                        }
                    }

                    foreach (var info in ReportPagesInfo)
                    {

                        foreach (var item in info.Items)
                        {

                            //fonts
                            iTextSharp.text.Font fontTitle = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                            iTextSharp.text.Font fontVersion = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                            iTextSharp.text.Font fontHeadersTable = FontFactory.GetFont("Arial", 11, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                            iTextSharp.text.Font fontNormal = FontFactory.GetFont("Arial", 11, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

                            fontVersion.SetStyle(2);

                            string tula = item.Tula.ToString();
                            string initDate = item.CheckinDate == null ? "EXTRA" : item.CheckinDate.ToShortDateString();
                            string tulaDate = item.ManifestDate == null ? "NO CLOSED" : item.ManifestDate.ToShortDateString();
                            string country = item.Country == null ? "EXTRA" : item.Country.ToString() == "BOGO" ? "BOGOTÁ" : item.Country.ToString() == "CALI" ? "CALI" : item.Country == "MEDE" ? "MEDELLÍN" : "BARRANQUILLA";

                            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(System.Web.Hosting.HostingEnvironment.MapPath("~/Content/Images/iron_brand.jfif"));
                            jpg.ScaleToFit(120f, 102f);

                            iTextSharp.text.Image frame = iTextSharp.text.Image.GetInstance(System.Web.Hosting.HostingEnvironment.MapPath("~/Content/Images/frame.png"));
                            frame.ScaleToFit(120f, 102f);
                            frame.SetAbsolutePosition(447, 723);

                            Paragraph ptitle = new Paragraph("ACTA INICIAL PROCESO DIGITALIZACIÓN", fontTitle);
                            ptitle.Alignment = Element.ALIGN_CENTER;

                            #region cero table

                            PdfPTable tableHeader = new PdfPTable(6);
                            tableHeader.DefaultCell.Border = 0;

                            PdfPCell cellJPG = new PdfPCell(jpg);
                            cellJPG.Border = 0;
                            cellJPG.Colspan = 5;
                            tableHeader.AddCell(cellJPG);


                            PdfPCell cellVersion = new PdfPCell();
                            cellVersion.Border = 0;
                            cellVersion.HorizontalAlignment = Element.ALIGN_CENTER;
                            cellVersion.VerticalAlignment = Element.ALIGN_BOTTOM;

                            cellVersion.AddElement(new Chunk("Versión 1-2015", fontVersion));
                            tableHeader.AddCell(cellVersion);

                            #endregion

                            #region first table

                            PdfPTable tableEntidad = new PdfPTable(5);
                            tableEntidad.HorizontalAlignment = Element.ALIGN_CENTER;

                            PdfPCell entidad = new PdfPCell();
                            entidad.Colspan = 2;
                            entidad.HorizontalAlignment = Element.ALIGN_CENTER;
                            entidad.AddElement(new Chunk("ENTIDAD PRODUCTORA DE LA DOCUMENTACIÓN Y/O ARCHIVOS", fontHeadersTable));
                            tableEntidad.AddCell(entidad);

                            PdfPCell serie = new PdfPCell();
                            serie.HorizontalAlignment = Element.ALIGN_CENTER;
                            serie.AddElement(new Chunk("SERIE DOCUMENTAL", fontHeadersTable));
                            tableEntidad.AddCell(serie);

                            PdfPCell ciudad = new PdfPCell();
                            ciudad.HorizontalAlignment = Element.ALIGN_CENTER;
                            ciudad.AddElement(new Chunk("CIUDAD", fontHeadersTable));
                            tableEntidad.AddCell(ciudad);

                            PdfPCell idate = new PdfPCell();
                            idate.HorizontalAlignment = Element.ALIGN_CENTER;
                            idate.AddElement(new Chunk("FECHA INICIAL", fontHeadersTable));
                            tableEntidad.AddCell(idate);

                            PdfPCell entidadr = new PdfPCell();
                            entidadr.Colspan = 2;
                            entidadr.AddElement(new Chunk("Grupo Bancolombia", fontNormal));
                            tableEntidad.AddCell(entidadr);

                            PdfPCell mover = new PdfPCell();
                            mover.AddElement(new Chunk("Movimiento diario Oficina", fontNormal));
                            tableEntidad.AddCell(mover);

                            PdfPCell countryr = new PdfPCell();
                            countryr.AddElement(new Chunk(country.ToString(), fontNormal));
                            tableEntidad.AddCell(countryr);

                            PdfPCell idater = new PdfPCell();
                            idater.AddElement(new Chunk(initDate.ToString(), fontNormal));
                            tableEntidad.AddCell(idater);

                            #endregion

                            #region second table

                            PdfPTable tableDocument = new PdfPTable(3);
                            tableEntidad.HorizontalAlignment = Element.ALIGN_CENTER;


                            PdfPCell cellOnly = new PdfPCell(new Phrase(new Chunk("DOCUMENTO INICIAL", fontHeadersTable)));
                            cellOnly.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cellOnly.Rowspan = 5;
                            tableDocument.AddCell(cellOnly);

                            tableDocument.AddCell("Tipo Documental");
                            tableDocument.AddCell("NOTAS VARIAS REZAGOS OFICINA 255-PREMIUM PLAZA-MEDELLÍN-2");
                            tableDocument.AddCell("Cod. Barra");
                            tableDocument.AddCell(tula);
                            tableDocument.AddCell("Oficina");
                            tableDocument.AddCell("255-PREMIUM PLAZA-MEDELLÍN");
                            tableDocument.AddCell("Fecha");
                            tableDocument.AddCell(tulaDate);
                            tableDocument.AddCell("Ruta inicial imágenes");
                            tableDocument.AddCell("Red de Iron, dirección ip \\firmado (\\10.51.37.50)BANCOLOMBIA");

                            #endregion

                            #region third table

                            PdfPTable tableCharacter = new PdfPTable(3);
                            tableCharacter.HorizontalAlignment = Element.ALIGN_CENTER;


                            cellOnly = new PdfPCell(new Phrase(new Chunk("CARACTERÍSTICAS TÉCNICAS DEL EQUIPO", fontHeadersTable)));
                            cellOnly.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cellOnly.Rowspan = 3;
                            tableCharacter.AddCell(cellOnly);

                            tableCharacter.AddCell("Equipo Scanner");
                            tableCharacter.AddCell("Kodak i 4200");
                            tableCharacter.AddCell("Serie(s):");
                            tableCharacter.AddCell("ID KC13161345 - ID KC13161042 - ID KC13161345 - ID KC13170042");
                            tableCharacter.AddCell("Tecnología de digitalización");
                            tableCharacter.AddCell("CCD");

                            #endregion

                            #region fourth table
                            PdfPTable tableCharacterImgs = new PdfPTable(3);
                            tableCharacterImgs.HorizontalAlignment = Element.ALIGN_CENTER;


                            cellOnly = new PdfPCell(new Phrase(new Chunk("CARACTERÍSTICAS TÉCNICAS DE LAS IMÁGENES", fontHeadersTable)));
                            cellOnly.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cellOnly.Rowspan = 3;
                            tableCharacterImgs.AddCell(cellOnly);

                            tableCharacterImgs.AddCell("Resolución de salida");
                            tableCharacterImgs.AddCell("150 dpi");
                            tableCharacterImgs.AddCell("Formato de archivo");
                            tableCharacterImgs.AddCell("PDF");
                            tableCharacterImgs.AddCell("Intensidad");
                            tableCharacterImgs.AddCell("Escala de grises");

                            #endregion

                            #region fifth table
                            PdfPTable tableDescription = new PdfPTable(1);
                            tableDescription.HorizontalAlignment = Element.ALIGN_JUSTIFIED;

                            PdfPCell cellDesc = new PdfPCell();
                            cellDesc.Padding = 10f;
                            cellDesc.AddElement(new Chunk("OBSERVACIONES: ", fontNormal));
                            cellDesc.AddElement(new Chunk("• Los Scanner Kodak i 4200 cuentan con los estándares de calidad y calibración según se evidencia en la certificación anexa a esta acta.: ", fontNormal));
                            cellDesc.AddElement(new Chunk("• Todos los registros que tengan el consecutivo de acta en el aplicativo DRC2 - campo 'Número de acta' 3T052207, son los asociados a esta acta. Las cantidades de archivos multipagina y de imágenes coinciden con los registros en este documento.", fontNormal));
                            tableDescription.AddCell(cellDesc);

                            #endregion

                            #region sixth table
                            PdfPTable tableFirm = new PdfPTable(2);
                            tableFirm.DefaultCell.Border = 0;
                            tableFirm.HorizontalAlignment = Element.ALIGN_CENTER;


                            tableFirm.AddCell(new Phrase(new Chunk("FIRMA DIRECTOR CENTRO LOGÍSTICO:", fontHeadersTable)));
                            tableFirm.AddCell(Environment.NewLine);


                            tableFirm.AddCell(new Phrase(new Chunk("NATALIA JIMENEZ CEBALLOS", fontHeadersTable)));
                            tableFirm.AddCell("_____________________________");

                            tableFirm.AddCell(Environment.NewLine);
                            tableFirm.AddCell(Environment.NewLine);

                            tableFirm.AddCell(new Phrase(new Chunk("FIRMA DIGITALIZADOR (ES):", fontHeadersTable)));
                            tableFirm.AddCell(Environment.NewLine);


                            tableFirm.AddCell(new Phrase(new Chunk("ALEXANDER BEDOYA AGUDELO", fontHeadersTable)));
                            tableFirm.AddCell("_____________________________");

                            tableFirm.AddCell("");
                            tableFirm.AddCell("_____________________________");
                            tableFirm.AddCell("");
                            tableFirm.AddCell("_____________________________");

                            #endregion

                            pdfDocument.Add(frame);
                            pdfDocument.Add(ptitle);
                            pdfDocument.Add(tableHeader);
                            pdfDocument.Add(tableEntidad);
                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            pdfDocument.Add(tableDocument);
                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            pdfDocument.Add(tableCharacter);
                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            pdfDocument.Add(tableCharacterImgs);
                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            pdfDocument.Add(tableDescription);
                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            pdfDocument.Add(tableFirm);


                            pdfDocument.NewPage();
                        }

                    }

                    // Save File
                    pdfDocument.Close();
                    byte[] byteInfo = pdfOutput.ToArray();
                    pdfOutput.Write(byteInfo, 0, byteInfo.Length);
                    //pdfOutput.Flush();
                    pdfOutput.Position = 0;
                    CoverSheetFile = pdfOutput;
                    pdfOutput = null;
                    retval = true;
                }
            }
            catch (Exception ex)
            {
                retval = false;
                Errors.Add(ex.Message);
            }

            return retval;
        }

        public bool GenerateByTID()
        {
            List<string> batchesName = new List<string>();
            bool retval = false;
            try
            {
                if (!_configLoaded)
                {
                    Errors.Add("Configuration not loaded.");
                }

                if (_configLoaded)
                {

                    BarcodeImages = new List<System.Drawing.Image>();
                    Document pdfDocument = null;
                    pdfDocument = new Document(PageSize.A4, 30f, 0f, 0f, 0f);

                    string tmpPdfFile = Path.Combine(TempPath, Guid.NewGuid().ToString()) + ".pdf";
                    MemoryStream pdfOutput = new MemoryStream();
                    PdfWriter writer = PdfWriter.GetInstance(pdfDocument, pdfOutput);
                    writer.CloseStream = false;
                    pdfDocument.Open();

                    foreach (var info in CoverPagesInfoTID)
                    {
                        foreach (var item in info.Items)
                        {

                            IDictionary<string, string> records = new Dictionary<string, string>();
                            //records.Add("Tula", item.PrecintoTula.ToString());
                            //records.Add("Precinto", item.Precinto.ToString());
                            //records.Add("Oficina", item.Codeffice == null ? "" : item.Codeffice.ToString());
                            //records.Add("Descripcion Oficina", item.NameOffice == null ? "" : item.NameOffice.ToString());
                            //records.Add("Sucursal", item.Sucursal == null ? "" : item.Sucursal.ToString() == "BOGO" ? "BOGOTA" : item.Sucursal.ToString() == "CALI" ? "CALI" : item.Sucursal == "MEDE" ? "MEDELLIN" : "BARRANQUILLA");

                            BarcodeLib.Barcode b = new BarcodeLib.Barcode();
                            b.BackColor = Color.White;
                            b.ForeColor = Color.Black;
                            b.IncludeLabel = true;
                            b.LabelPosition = LabelPositions.BOTTOMCENTER;
                            System.Drawing.Font font = new System.Drawing.Font("Calibri", 25, FontStyle.Bold);
                            b.LabelFont = font;
                            b.Height = 120;
                            b.Width = 800;

                            System.Drawing.Image dossier_bc = b.Encode(type, string.Format("TID-{0}", item.TIDID.ToString()));

                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            pdfDocument.Add(new Paragraph(Environment.NewLine));

                            iTextSharp.text.Image ibc = iTextSharp.text.Image.GetInstance(dossier_bc, BaseColor.WHITE);
                            ibc.Alignment = Element.ALIGN_LEFT;
                            ibc.ScalePercent(DocumentTypePercent);
                            pdfDocument.Add(ibc);

                            for (int i = 0; i <= 10; i++)
                            {
                                pdfDocument.Add(new Paragraph(Environment.NewLine));
                            }


                            foreach (var record in records)
                            {
                                string line1 = record.Key + ": " + record.Value + Environment.NewLine;
                                var pr = new Paragraph(new Phrase(line1, FontFactory.GetFont("Calibri", "", true, 19, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)));
                                pr.Alignment = Element.ALIGN_CENTER;
                                pdfDocument.Add(pr);
                            }

                            if (!String.IsNullOrEmpty(PathIVImageLocation))
                            {
                                ibc = iTextSharp.text.Image.GetInstance(PathIVImageLocation); //"Separator"
                                ibc.SetAbsolutePosition(50 , 300);
                                ibc.ScalePercent(SeparatorPercent);
                                ibc.RotationDegrees = 90f;
                                pdfDocument.Add(ibc);

                                // ibc = iTextSharp.text.Image.GetInstance(PathIVImageLocation); //"Separator"                        
                                //// ibc.SetAbsolutePosition(SeparatorX - 110, SeparatorY - 850);
                                // ibc.ScalePercent(SeparatorPercent);
                                // ibc.RotationDegrees = 90f;
                                // pdfDocument.Add(ibc);
                            }

                            string line = "                                    TID: " + item.TIDID + Environment.NewLine;
                            var p = new Paragraph(new Phrase(line, FontFactory.GetFont("Arial", "", true, 14, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)));
                            p.Alignment = Element.ALIGN_LEFT;
                            pdfDocument.Add(p);

                            line = "                                    SKP Box Number  : " + item.SKPBoXNumber + Environment.NewLine;
                            var p1 = new Paragraph(new Phrase(line, FontFactory.GetFont("Arial", "", true, 14, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)));
                            p1.Alignment = Element.ALIGN_LEFT;
                            pdfDocument.Add(p1);

                            line = "                                    Service Area  : " + item.ServiceArea + Environment.NewLine;
                            var p2 = new Paragraph(new Phrase(line, FontFactory.GetFont("Arial", "", true, 14, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)));
                            p2.Alignment = Element.ALIGN_LEFT;
                            pdfDocument.Add(p2);

                            line = "                                    Sub Service Area  : " + item.SubServiceArea + Environment.NewLine;
                            var p3 = new Paragraph(new Phrase(line, FontFactory.GetFont("Arial", "", true, 14, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)));
                            p3.Alignment = Element.ALIGN_LEFT;
                            pdfDocument.Add(p3);

                            line = "                                    Finance Code : " + item.FinanceCode + Environment.NewLine;
                            var p4 = new Paragraph(new Phrase(line, FontFactory.GetFont("Arial", "", true, 14, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)));
                            p4.Alignment = Element.ALIGN_LEFT;
                            pdfDocument.Add(p4);
                            // Add ne page.
                            pdfDocument.NewPage();
                        }
                    }



                    // Save File
                    pdfDocument.Close();
                    byte[] byteInfo = pdfOutput.ToArray();
                    pdfOutput.Write(byteInfo, 0, byteInfo.Length);
                    //pdfOutput.Flush();
                    pdfOutput.Position = 0;
                    CoverSheetFile = pdfOutput;
                    pdfOutput = null;
                    retval = true;
                }
            }
            catch (Exception ex)
            {
                retval = false;
                Errors.Add(ex.Message);
            }

            return retval;
        }

        public bool GenerateByBatches()
        {
            List<string> batchesName = new List<string>();
            bool retval = false;
            try
            {
                if (!_configLoaded)
                {
                    Errors.Add("Configuration not loaded.");
                }

                if (_configLoaded)
                {

                    BarcodeImages = new List<System.Drawing.Image>();
                    Document pdfDocument = null;
                   // pdfDocument = new Document(PageSize.A4, 5f, 5f, 15f, 15f);
                    pdfDocument = new Document(PageSize.A4, 30f, 0f, 0f, 0f);

                    string tmpPdfFile = Path.Combine(TempPath, Guid.NewGuid().ToString()) + ".pdf";
                    MemoryStream pdfOutput = new MemoryStream();
                    PdfWriter writer = PdfWriter.GetInstance(pdfDocument, pdfOutput);
                    writer.CloseStream = false;
                    pdfDocument.Open();

                    foreach (var info in CoverPagesInfoBatch)
                    {
                        foreach (var item in info.Items)
                        {


                            //create CoverSheet of Batch
                            batchesName.Add(item.BatchName);

                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            pdfDocument.Add(new Paragraph(Environment.NewLine));



                            BarcodeLib.Barcode bBatch = new BarcodeLib.Barcode();
                            bBatch.BackColor = Color.White;
                            bBatch.ForeColor = Color.Black;
                            bBatch.IncludeLabel = true;
                            bBatch.LabelPosition = LabelPositions.BOTTOMCENTER;
                            bBatch.LabelFont = new System.Drawing.Font("Calibri", 25, FontStyle.Bold);
                            bBatch.Height = 120;
                            bBatch.Width = 1000;

                            System.Drawing.Image dossier_bcBatch = bBatch.Encode(type, string.Format("{0}", item.BatchName.ToString()));
                            iTextSharp.text.Image ibcBatch = iTextSharp.text.Image.GetInstance(dossier_bcBatch, BaseColor.WHITE);
                            ibcBatch.Alignment = Element.ALIGN_LEFT;
                            ibcBatch.ScalePercent(DocumentTypePercent);
                            pdfDocument.Add(ibcBatch);

                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            //pdfDocument.Add(new Paragraph(Environment.NewLine));
                            //pdfDocument.Add(new Paragraph(Environment.NewLine));
                            //pdfDocument.Add(new Paragraph(Environment.NewLine));

                            
                            string line = "                Batch Name: " + item.BatchName + Environment.NewLine;
                            line.PadLeft(1000);
                            var p = new Paragraph(new Phrase(line, FontFactory.GetFont("Arial", "", true, 14, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)));
                            p.Alignment = Element.ALIGN_MIDDLE;
                            pdfDocument.Add(p);

                            line = "                SKP ID  : " + item.SkpBox + Environment.NewLine;
                            var p1 = new Paragraph(new Phrase(line, FontFactory.GetFont("Arial", "", true, 14, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)));
                            p1.Alignment = Element.ALIGN_MIDDLE;
                            pdfDocument.Add(p1);

                            line = "                Project Name: " + "Lambeth Borough Council" + Environment.NewLine;
                            var p2 = new Paragraph(new Phrase(line, FontFactory.GetFont("Arial", "", true, 14, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)));
                            p2.Alignment = Element.ALIGN_MIDDLE;
                            pdfDocument.Add(p2);

                            

                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            pdfDocument.Add(new Paragraph(Environment.NewLine));

                            

                            //BarcodeLib.Barcode bBatchDgt = new BarcodeLib.Barcode();
                            //bBatchDgt.BackColor = Color.White;
                            //bBatchDgt.ForeColor = Color.Black;
                            //bBatchDgt.IncludeLabel = true;
                            //bBatchDgt.LabelPosition = LabelPositions.BOTTOMCENTER;
                            //bBatchDgt.LabelFont = new System.Drawing.Font("Calibri", 30, FontStyle.Bold);
                            //bBatchDgt.Height = 180;
                            //bBatchDgt.Width = 600;

                            //System.Drawing.Image dossier_bcBatchDgt = bBatchDgt.Encode(type, string.Format("{0}", item.BatchName.Substring(0, 10).ToString()));
                            //iTextSharp.text.Image ibcBatchDgt = iTextSharp.text.Image.GetInstance(dossier_bcBatchDgt, BaseColor.WHITE);
                            //ibcBatchDgt.Alignment = Element.ALIGN_CENTER;
                            //ibcBatchDgt.ScalePercent(DocumentTypePercent);
                            //pdfDocument.Add(ibcBatchDgt);


                            pdfDocument.NewPage();


                        }
                    }



                    // Save File
                    pdfDocument.Close();
                    byte[] byteInfo = pdfOutput.ToArray();
                    pdfOutput.Write(byteInfo, 0, byteInfo.Length);
                    //pdfOutput.Flush();
                    pdfOutput.Position = 0;
                    CoverSheetFile = pdfOutput;
                    pdfOutput = null;
                    retval = true;
                }
            }
            catch (Exception ex)
            {
                retval = false;
                Errors.Add(ex.Message);
            }

            return retval;
        }

        public bool GenerateByFOPEP()
        {
            List<string> batchesName = new List<string>();
            bool retval = false;
            try
            {
                if (!_configLoaded)
                {
                    Errors.Add("Configuration not loaded.");
                }

                if (_configLoaded)
                {

                    BarcodeImages = new List<System.Drawing.Image>();
                    Document pdfDocument = null;
                    pdfDocument = new Document(PageSize.A4, 5f, 5f, 15f, 15f);

                    string tmpPdfFile = Path.Combine(TempPath, Guid.NewGuid().ToString()) + ".pdf";
                    MemoryStream pdfOutput = new MemoryStream();
                    PdfWriter writer = PdfWriter.GetInstance(pdfDocument, pdfOutput);
                    writer.CloseStream = false;
                    pdfDocument.Open();

                    foreach (var info in CoverPagesInfoFOPEP)
                    {
                        foreach (var item in info.Items)
                        {

                            IDictionary<string, string> records = new Dictionary<string, string>();
                            records.Add("Tula", item.PrecintoTula.ToString());
                            records.Add("Precinto", item.Precinto.ToString());
                            records.Add("Oficina", item.Codeffice == null ? "" : item.Codeffice.ToString());
                            records.Add("Descripcion Oficina", item.NameOffice == null ? "" : item.NameOffice.ToString());
                            records.Add("Sucursal", item.Sucursal == null ? "" : item.Sucursal.ToString() == "BOGO" ? "BOGOTA" : item.Sucursal.ToString() == "CALI" ? "CALI" : item.Sucursal == "MEDE" ? "MEDELLIN" : "BARRANQUILLA");

                            BarcodeLib.Barcode b = new BarcodeLib.Barcode();
                            b.BackColor = Color.White;
                            b.ForeColor = Color.Black;
                            b.IncludeLabel = true;
                            b.LabelPosition = LabelPositions.BOTTOMCENTER;
                            System.Drawing.Font font = new System.Drawing.Font("Calibri", 30, FontStyle.Bold);
                            b.LabelFont = font;
                            b.Height = 200;
                            b.Width = 700;

                            System.Drawing.Image dossier_bc = b.Encode(type, string.Format("TID-{0}", item.TIDID.ToString()));

                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            pdfDocument.Add(new Paragraph(Environment.NewLine));
                            pdfDocument.Add(new Paragraph(Environment.NewLine));

                            iTextSharp.text.Image ibc = iTextSharp.text.Image.GetInstance(dossier_bc, BaseColor.WHITE);
                            ibc.Alignment = Element.ALIGN_CENTER;
                            ibc.ScalePercent(DocumentTypePercent);
                            pdfDocument.Add(ibc);

                            for (int i = 0; i <= 6; i++)
                            {
                                pdfDocument.Add(new Paragraph(Environment.NewLine));
                            }


                            foreach (var record in records)
                            {
                                string line = record.Key + ": " + record.Value + Environment.NewLine;
                                var p = new Paragraph(new Phrase(line, FontFactory.GetFont("Calibri", "", true, 19, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)));
                                p.Alignment = Element.ALIGN_CENTER;
                                pdfDocument.Add(p);
                            }

                            for (int i = 0; i <= 3; i++)
                            {
                                pdfDocument.Add(new Paragraph(Environment.NewLine));
                            }


                            System.Drawing.Image dossier_bc2 = b.Encode(type, string.Format("{0}", item.fopep.ToString()));

                            iTextSharp.text.Image ibc2 = iTextSharp.text.Image.GetInstance(dossier_bc2, BaseColor.WHITE);
                            ibc2.Alignment = Element.ALIGN_CENTER;
                            ibc2.ScalePercent(DocumentTypePercent);
                            pdfDocument.Add(ibc2);

                            // Add ne page.
                            pdfDocument.NewPage();
                        }
                    }



                    // Save File
                    pdfDocument.Close();
                    byte[] byteInfo = pdfOutput.ToArray();
                    pdfOutput.Write(byteInfo, 0, byteInfo.Length);
                    //pdfOutput.Flush();
                    pdfOutput.Position = 0;
                    CoverSheetFile = pdfOutput;
                    pdfOutput = null;
                    retval = true;
                }
            }
            catch (Exception ex)
            {
                retval = false;
                Errors.Add(ex.Message);
            }

            return retval;
        }


    }

    public class BarcodeConfiguration
    {
        public bool IncludeLabel = true;
        public string Type = "CODE 128";
        public int Width = 200;
        public int Height = 100;
        public int Top = 400;
        public int Left = 50;
        public AlignmentPositions AlignmentPosition;
    }

    public class PageDataConfiguration
    {
        public float FontSize = 20f;
        public int LineSeparationSpace = 25;
        public int Top = 410;
        public int Left = 220;
    }

    public class CoverSheetPageTID
    {
        public CoverSheetPageTID()
        {
            Items = new List<CoversheetModel>();
        }
        public IList<CoversheetModel> Items { get; set; }
    }

    public class CoverSheetPageReport
    {
        public CoverSheetPageReport()
        {
            Items = new List<DigitalizationReportModel>();
        }
        public IList<DigitalizationReportModel> Items { get; set; }
    }

    public class CoverSheetPageBatch
    {
        public CoverSheetPageBatch()
        {
            Items = new List<CoverSheetBatch>();
        }
        public IList<CoverSheetBatch> Items { get; set; }
    }

    public class CoverSheetPageFOPEP
    {
        public CoverSheetPageFOPEP()
        {
            Items = new List<CoversheetFOPEP>();
        }
        public IList<CoversheetFOPEP> Items { get; set; }
    }

    public class CoverSheetPageBoth
    {
        public CoverSheetPageBoth()
        {
            ItemsTid = new List<CoversheetModel>();
            ItemsPID = new List<PID>();


        }
        public IList<CoversheetModel> ItemsTid { get; set; }
        public IList<PID> ItemsPID { get; set; }

        public static implicit operator List<object>(CoverSheetPageBoth v)
        {
            throw new NotImplementedException();
        }
    }

    public class CoverSheetInfo
    {
        public CoverSheetInfo()
        {
            Items = new List<dynamic>();
        }
        public List<dynamic> Items { get; set; }
    }
}