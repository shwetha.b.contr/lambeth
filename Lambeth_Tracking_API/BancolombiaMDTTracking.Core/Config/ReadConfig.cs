﻿using BancolombiaMDTTracking.Core.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BancolombiaMDTTracking.Core.Config
{
    public static class ReadConfig
    {
        #region properties
        public static string DatabaseNameJob;
        public static string BatchClassName;
        public static string PathMMFile;
        public static string Environment;
        public static string MaxNumberofDocFields;
        public static string MaxNumberofBatchFields;
        public static string MaxNumberofMiscFields;
        public static string NumberofColumns;
        public static string DOCUMENT_FIELD_PREFIX = "DOC_INDEX_";
        public static string BATCH_FIELD_PREFIX = "BATCH_INDEX_";
        public static string MISCELLANEOUS_FIELD_PREFIX = "MISC_";
        public static string UNKNOWN_FIELD_PREFIX = "UNKNOWN_INDEX_";
        private static List<XElement> _Releases;
        public static string PathImages;
        public static string PhysicalPathThumbnails;
        public static string PathThumbnails;
        public static string PathPullImages;
        public static string PathDumyImage;
        public static string ImageDumyThumb_Base64;
        public static string LicenseGDPicture;


        public static List<XElement> Releases
        {
            get
            {
                XDocument doc = new XDocument();
                string folder = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, System.IO.Path.Combine("XMLTemplate", "JobConfig.xml"));  
                doc = XDocument.Load(folder);
                _Releases = doc.Element("StandardFTPConfiguration").Elements("add").FirstOrDefault(x => x.Attribute("key").Value == "Releases").Elements("Release").ToList();
                return _Releases;

            }
        }
        private static List<XElement> _ExtendedProperties;
        public static List<XElement> ExtendedProperties
        {
            get
            {
                XDocument doc = new XDocument();
                string folder = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, System.IO.Path.Combine("XMLTemplate", "JobConfig.xml"));
                doc = XDocument.Load(folder);
                _ExtendedProperties = doc.Element("StandardFTPConfiguration").Elements("add").FirstOrDefault(x => x.Attribute("key").Value == "ExtendedProperties").Elements("ExtendedProperty").ToList();
                return _ExtendedProperties;

            }
        }
        #endregion

        #region Coversheet properties
        public static string PrintType;
        public static int Barcode_DPI;
        public static string Include_CoverSheetPatchSeparator;
        public static string csBarCode_hor_DocumentTypeText;
        public static float csBarCode_hor_DocumentTypePercent;
        public static float csBarCode_hor_DocumentTypeX;
        public static float csBarCode_hor_DocumentTypeY;
        public static string csBarCode_hor_SeparatorText;
        public static float csBarCode_hor_SeparatorPercent;
        public static float csBarCode_hor_SeparatorX;
        public static float csBarCode_hor_SeparatorY;
        public static int csBarCode_hor_Left;
        public static int csBarCode_hor_Top;
        public static int csBarCode_hor_Width;
        public static int csBarCode_hor_Height;
        public static string csBarCode_hor_Type;
        public static float csPageData_hor_FontSize;
        public static int csPageData_hor_Left;
        public static int csPageData_hor_LineSeparationSpace;
        public static int csPageData_hor_Top;
        public static string csBarCode_ver_DocumentTypeText;
        public static float csBarCode_ver_DocumentTypePercent;
        public static float csBarCode_ver_DocumentTypeX;
        public static float csBarCode_ver_DocumentTypeY;
        public static string csBarCode_ver_SeparatorText;
        public static float csBarCode_ver_SeparatorPercent;
        public static float csBarCode_ver_SeparatorX;
        public static float csBarCode_ver_SeparatorY;
        public static int csBarCode_ver_Left;
        public static int csBarCode_ver_Top;
        public static int csBarCode_ver_Width;
        public static int csBarCode_ver_Height;
        public static string csBarCode_ver_Type;
        public static float csPageData_ver_FontSize;
        public static int csPageData_ver_Left;
        public static int csPageData_ver_LineSeparationSpace;
        public static int csPageData_ver_Top;
        #endregion

        public static void init()
        {
            DatabaseNameJob = ConfigurationManager.AppSettings["DatabaseNameJob"];
            BatchClassName = ConfigurationManager.AppSettings["BatchClassName"];
            PathMMFile = ConfigurationManager.AppSettings["PathMMFile"];
            Environment = ConfigurationManager.AppSettings["Environment"];

            MaxNumberofDocFields = ConfigurationManager.AppSettings["MaxNumberofDocFields"];
            MaxNumberofBatchFields = ConfigurationManager.AppSettings["MaxNumberofBatchFields"];
            MaxNumberofMiscFields = ConfigurationManager.AppSettings["MaxNumberofMiscFields"];

            NumberofColumns = ConfigurationManager.AppSettings["NumberofColumns"];

            // Coversheet
            PrintType = ConfigurationManager.AppSettings["PrintType"].ToString();
            Barcode_DPI = Convert.ToInt32(ConfigurationManager.AppSettings["Barcode_DPI"].ToString());
            Include_CoverSheetPatchSeparator = ConfigurationManager.AppSettings["Include_CoverSheetPatchSeparator"].ToString();
            csBarCode_hor_DocumentTypeText = ConfigurationManager.AppSettings["csBarCode_hor_DocumentTypeText"].ToString();
            csBarCode_hor_DocumentTypePercent = Convert.ToSingle(ConfigurationManager.AppSettings["csBarCode_hor_DocumentTypePercent"]);
            csBarCode_hor_DocumentTypeX = Convert.ToSingle(ConfigurationManager.AppSettings["csBarCode_hor_DocumentTypeX"]);
            csBarCode_hor_DocumentTypeY = Convert.ToSingle(ConfigurationManager.AppSettings["csBarCode_hor_DocumentTypeY"]);

            csBarCode_hor_SeparatorText = ConfigurationManager.AppSettings["csBarCode_hor_SeparatorText"].ToString();
            csBarCode_hor_SeparatorPercent = Convert.ToSingle(ConfigurationManager.AppSettings["csBarCode_hor_SeparatorPercent"]);
            csBarCode_hor_SeparatorX = Convert.ToSingle(ConfigurationManager.AppSettings["csBarCode_hor_SeparatorX"]);
            csBarCode_hor_SeparatorY = Convert.ToSingle(ConfigurationManager.AppSettings["csBarCode_hor_SeparatorY"]);

            csBarCode_hor_Left = Convert.ToInt16(ConfigurationManager.AppSettings["csBarCode_hor_Left"]);
            csBarCode_hor_Top = Convert.ToInt16(ConfigurationManager.AppSettings["csBarCode_hor_Top"]);
            csBarCode_hor_Width = Convert.ToInt16(ConfigurationManager.AppSettings["csBarCode_hor_Width"]);
            csBarCode_hor_Height = Convert.ToInt16(ConfigurationManager.AppSettings["csBarCode_hor_Height"]);
            csBarCode_hor_Type = ConfigurationManager.AppSettings["csBarCode_hor_Type"].ToString();

            csPageData_hor_FontSize = float.Parse(ConfigurationManager.AppSettings["csPageData_hor_FontSize"]);
            csPageData_hor_Left = Convert.ToInt16(ConfigurationManager.AppSettings["csPageData_hor_Left"]);
            csPageData_hor_LineSeparationSpace = Convert.ToInt16(ConfigurationManager.AppSettings["csPageData_hor_LineSeparationSpace"]);
            csPageData_hor_Top = Convert.ToInt16(ConfigurationManager.AppSettings["csPageData_hor_Top"]);

            csBarCode_ver_DocumentTypeText = ConfigurationManager.AppSettings["csBarCode_ver_DocumentTypeText"].ToString();
            csBarCode_ver_DocumentTypePercent = Convert.ToSingle(ConfigurationManager.AppSettings["csBarCode_ver_DocumentTypePercent"]);
            csBarCode_ver_DocumentTypeX = Convert.ToSingle(ConfigurationManager.AppSettings["csBarCode_ver_DocumentTypeX"]);
            csBarCode_ver_DocumentTypeY = Convert.ToSingle(ConfigurationManager.AppSettings["csBarCode_ver_DocumentTypeY"]);

            csBarCode_ver_SeparatorText = ConfigurationManager.AppSettings["csBarCode_ver_SeparatorText"].ToString();
            csBarCode_ver_SeparatorPercent = Convert.ToSingle(ConfigurationManager.AppSettings["csBarCode_ver_SeparatorPercent"]);
            csBarCode_ver_SeparatorX = Convert.ToSingle(ConfigurationManager.AppSettings["csBarCode_ver_SeparatorX"]);
            csBarCode_ver_SeparatorY = Convert.ToSingle(ConfigurationManager.AppSettings["csBarCode_ver_SeparatorY"]);

            csBarCode_ver_Left = Convert.ToInt16(ConfigurationManager.AppSettings["csBarCode_ver_Left"]);
            csBarCode_ver_Top = Convert.ToInt16(ConfigurationManager.AppSettings["csBarCode_ver_Top"]);
            csBarCode_ver_Width = Convert.ToInt16(ConfigurationManager.AppSettings["csBarCode_ver_Width"]);
            csBarCode_ver_Height = Convert.ToInt16(ConfigurationManager.AppSettings["csBarCode_ver_Height"]);
            csBarCode_ver_Type = ConfigurationManager.AppSettings["csBarCode_ver_Type"].ToString();

            csPageData_ver_FontSize = float.Parse(ConfigurationManager.AppSettings["csPageData_ver_FontSize"]);
            csPageData_ver_Left = Convert.ToInt16(ConfigurationManager.AppSettings["csPageData_ver_Left"]);
            csPageData_ver_LineSeparationSpace = Convert.ToInt16(ConfigurationManager.AppSettings["csPageData_ver_LineSeparationSpace"]);
            csPageData_ver_Top = Convert.ToInt16(ConfigurationManager.AppSettings["csPageData_ver_Top"]);

            PhysicalPathThumbnails = ConfigurationManager.AppSettings["PhysicalPathThumbnails"];
            PathImages = ConfigurationManager.AppSettings["PathImages"];
            PathThumbnails = ConfigurationManager.AppSettings["PathThumbnails"];
            PathPullImages = ConfigurationManager.AppSettings["PathPullImages"];
            PathDumyImage = ConfigurationManager.AppSettings["PathDumyImage"];
            LicenseGDPicture = ConfigurationManager.AppSettings["LicenseGDPicture"];
        }

    }
}
