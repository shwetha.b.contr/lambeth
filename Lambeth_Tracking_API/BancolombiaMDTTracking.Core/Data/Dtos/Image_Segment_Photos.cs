﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancolombiaMDTTracking.Core.Dtos
{
    public class Image_Segment_Photos
    {
        public string Segment { get; set; }
        public List<string> Photos { get; set; }
        public long _TID { get; set; }
    }
}
