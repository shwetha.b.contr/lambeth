﻿using dbconnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancolombiaMDTTracking.Core.Dtos
{
    public class Images_SegmentDto
    {
        public List<Images_Segment> Segments { get; set; }
        public List<dynamic> SegmentTotals { get; set; }
    }
}
