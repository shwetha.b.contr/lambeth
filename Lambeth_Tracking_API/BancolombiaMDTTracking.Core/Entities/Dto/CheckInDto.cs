﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LambethTracking.Core.Entities.Dto
{
    public class CheckInDto
    {
        public int Id { get; set; }
        public string ItemCode { get; set; }
        public string Financecode { get; set; }
        public string ServiceArea { get; set; }
        public string SubServiceArea { get; set; }
        public string InsightValue { get; set; }
        public bool BoxExists { get; set; }
        public bool Available { get; set; }
        public int NumofBatches { get; set; }
        public string SKPBox { get; set; }
        public long User_ID { get; set; }
        public string UserName { get; set; }
        public DateTime DateTime { get; set; }
        public bool HoldBox { get; set; }
        public string Reasoncode { get; set; }
        public string Description { get; set; }
    }

    public class ExistingBoxDetails : CheckInDto
    {
        public string Message  { get; set; }

        public List<string> BoxIdList { get; set; }
    }
}
