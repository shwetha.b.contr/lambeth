﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancolombiaMDTTracking.Core.Entities.Dto
{
    public class E2EAuditDto
    {
        public long Taudit_id { get; set; }
        public string Customer_ID { get; set; }
        public string Project_ID { get; set; }
        public string Task { get; set; }
        public string Package_Name { get; set; }
        public string Unit_Type { get; set; }
        public int? Total_Units { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
        public string UserID { get; set; }
        public string SiteName { get; set; }
        public DateTime? LogDateTime { get; set; }
        public string CheckinId { get; set; }
        
    }
}
