﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancolombiaMDTTracking.Core.Entities.Dto
{
    public class TID_Dto
    {
       public int Id { get; set; }
       public int Checkin_Id { get; set; }
       public long TID { get; set; }
       public long User_ID { get; set; }
       public bool Available { get; set; }
       public string ItemCode { get; set; }
       public bool IsDeleted { get; set; }
    }
   
}