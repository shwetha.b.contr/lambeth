﻿using BancolombiaMDTTracking.Core.Entities.Dto;
using BancolombiaMDTTracking.Core.Factories;
using dbconnection;
using LambethTracking.Core.Entities.Dto;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancolombiaMDTTracking.Core.Repository
{
    public interface ICheckInRepository : IRepositoryBase<MasterInventory>
    {
        ExistingBoxDetails GetBoxDetails(string boxCode, string type);
        string SaveBoxDetails(CheckInDto CheckInDtoObj);
        HashSet<string> GetDocTypes();
        string SaveTids(string boxId, List<TID_Dto> tidDtoObj);
       // List<TID_Tbl> GetTid();
        long GetNewTid();
        string SaveTidsList(string boxId, string userName, string skpbox);
        HashSet<long> GetTidsList(string boxId);
        string DeleteTid(long tid);
        string SaveHoldBoxDetails(CheckInDto CheckInDtoObj);
        string CheckItemCodeFromExcelData(string boxCode);
        HashSet<string> GetBoxIdList(string tid);
    }

    public class CheckInRepository : RepositoryBase<MasterInventory>, ICheckInRepository
    {
        string connectionString = ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString;
        public CheckInRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
        public MasterInventory GetBoxDetails1(string boxCode, string type)
        {
            MasterInventory boxDetails = new MasterInventory();
            if (type != null && type == "box")
            {
                var query = new Sql()
                .Select("*")
                .From("dbo.MasterInventory")
                .Where("ItemCode = @0", boxCode);

                boxDetails = this.Context.SingleOrDefault<MasterInventory>(query);
            }
            else if (type != null && type == "finance")
            {
                var query = new Sql()
                .Select("*")
                .From("dbo.MasterInventory")
                .Where("Financecode = @0", boxCode);

                boxDetails = this.Context.FirstOrDefault<MasterInventory>(query);
            }
            

            return boxDetails;
        }

        public ExistingBoxDetails GetBoxDetails(string boxCode, string type)
        {
            ExistingBoxDetails boxDetails = new ExistingBoxDetails();
            CheckinTbl checkiTblObj = new CheckinTbl();
            MasterInventory masterInventoryObj = new MasterInventory();
            List<string> itemCodeLists = new List<string>();
            var checkInTblItemcodeCount = 0;

            
            if (type != null && type == "box")
            {
                //var itemCodeCountQuery = new Sql().Select("count(1)").From("dbo.Checkin_Tbl")
                //.Where("REPLACE(ItemCode, '~', '')  LIKE '%" + boxCode + "'");
                //checkInTblItemcodeCount = this.Context.ExecuteScalar<int>(itemCodeCountQuery);

                //if (checkInTblItemcodeCount == 1)
                //{
                      var query = new Sql()
                     .Select("*")
                     .From("dbo.Checkin_Tbl")
                     .Where("ItemCode = @0", boxCode);
                     checkiTblObj = this.Context.SingleOrDefault<CheckinTbl>(query);
                    if (checkiTblObj != null)
                    {
                        boxDetails.Id = checkiTblObj.Id;
                        boxDetails.ItemCode = checkiTblObj.ItemCode;
                        boxDetails.Financecode = checkiTblObj.Financecode;
                        boxDetails.ServiceArea = checkiTblObj.ServiceArea;
                        boxDetails.SubServiceArea = checkiTblObj.SubServiceArea;
                        boxDetails.InsightValue = checkiTblObj.InsightValue;
                        boxDetails.NumofBatches = checkiTblObj.NumofBatches;
                        boxDetails.BoxExists = checkiTblObj.BoxExists;
                        boxDetails.Available = checkiTblObj.Available;
                        boxDetails.Message = "CheckinTbl";
                        boxDetails.SKPBox = checkiTblObj.SKPBox;
                        boxDetails.User_ID = checkiTblObj.User_ID;
                    }
                //}
                //else if (checkInTblItemcodeCount > 1)
                //{
                //    itemCodeLists = this.GetBoxIdListsCheckin(boxCode);
                //    boxDetails.Message = "ExistingBoxIdList";
                //    boxDetails.BoxIdList = itemCodeLists;
                //}

            }
            if (type != null && type == "box" && checkiTblObj == null)
            {
                var itemCodeCountQuery = new Sql().Select("count(1)").From("dbo.MasterInventory")
                .Where("REPLACE(ItemCode, '~', '')  LIKE '%" + boxCode + "'");
                var itemcodeCount = this.Context.ExecuteScalar<int>(itemCodeCountQuery);

                if (itemcodeCount == 1)
                {
                    var query = new Sql()
                                 .Select("*")
                                 .From("dbo.MasterInventory")
                                 .Where("REPLACE(ItemCode, '~', '')  = @0", boxCode);

                    masterInventoryObj = this.Context.SingleOrDefault<MasterInventory>(query);
                    if (masterInventoryObj != null)
                    {
                        boxDetails.ItemCode = masterInventoryObj.ItemCode;
                        boxDetails.Financecode = masterInventoryObj.Financecode;
                        boxDetails.ServiceArea = masterInventoryObj.ServiceArea;
                        boxDetails.SubServiceArea = masterInventoryObj.SubServiceArea;
                        boxDetails.InsightValue = masterInventoryObj.InsightValue;
                        boxDetails.Message = "MasterInventory";
                    }
                }
                if (itemcodeCount > 1 || masterInventoryObj == null)
                {
                    itemCodeLists =  this.GetBoxIdListsMaster(boxCode);
                    boxDetails.Message = "ExistingBoxIdList";
                    boxDetails.BoxIdList = itemCodeLists;
                }

               
            }
                      
            if (type != null && type == "finance")
            {
                var query = new Sql()
                .Select("*")
                .From("dbo.MasterInventory")
                .Where("Financecode = @0", boxCode);
                masterInventoryObj = this.Context.FirstOrDefault<MasterInventory>(query);
                if (masterInventoryObj != null)
                {                  
                    boxDetails.Financecode = masterInventoryObj.Financecode;
                    boxDetails.ServiceArea = masterInventoryObj.ServiceArea;
                    boxDetails.SubServiceArea = masterInventoryObj.SubServiceArea;
                    boxDetails.InsightValue = masterInventoryObj.InsightValue;
                }
               
            }

            
            return boxDetails;
        }
        public string SaveBoxDetails(CheckInDto CheckInDtoObj)
        {
            string response = string.Empty;
            long user_Id = this.GetUserId(CheckInDtoObj.UserName);
            try {
                CheckinTbl CheckinTblObj = new CheckinTbl();
                CheckinTblObj.ItemCode = CheckInDtoObj.ItemCode;
                CheckinTblObj.Financecode = CheckInDtoObj.Financecode;
                CheckinTblObj.ServiceArea = CheckInDtoObj.ServiceArea;
                CheckinTblObj.SubServiceArea = CheckInDtoObj.SubServiceArea;
                CheckinTblObj.InsightValue = CheckInDtoObj.InsightValue;
                CheckinTblObj.BoxExists = CheckInDtoObj.BoxExists;
                CheckinTblObj.NumofBatches = CheckInDtoObj.NumofBatches;
                CheckinTblObj.Available = true;
                CheckinTblObj.SKPBox = CheckInDtoObj.SKPBox;
                CheckinTblObj.User_ID = user_Id;
                CheckinTblObj.DateTime = DateTime.Now;
                if (CheckInDtoObj.Id == 0)
                {
                    var x = this.Context.Insert(CheckinTblObj);
                    response = "Box details inserted successfully.";
                    CreateBatch(CheckInDtoObj.ItemCode, CheckInDtoObj.NumofBatches, CheckInDtoObj.SKPBox, user_Id);
                }
                else
                {
                    CheckinTblObj.Id = CheckInDtoObj.Id;
                    var x = this.Context.Update(CheckinTblObj);
                    var batch = new Batch_Label_TBL()
                    {
                        CheckinID = CheckInDtoObj.Id,
                        Available = true
                    };
                    this.DeleteBatchLable(CheckInDtoObj.Id);
                    CreateBatch(CheckInDtoObj.ItemCode, CheckInDtoObj.NumofBatches, CheckInDtoObj.SKPBox, user_Id);

                    response = "Box details updated successfully.";
                }

                int? checkinId = this.UpdateTidwithCheckin(CheckInDtoObj.ItemCode);
                if (checkinId != null)
                {

                    this.E2E_Update_IMTrack_TAudit_Working(checkinId);
                    this.E2E_Update_IMTrack_TAudit1(CheckinTblObj.ItemCode);
                }
            }
            catch(Exception ex)
            {
                response = ex.Message;
            }
                
                
            return response;
        }

        //using (SqlConnection connection = new SqlConnection(connectionString))
        //{
        //    try
        //    {
        //        connection.Open(); 
        //        CheckinTbl CheckinTblObj = new CheckinTbl();
        //       string itemCode = CheckInDtoObj.ItemCode;
        //       string financecode = CheckInDtoObj.Financecode;
        //       string serviceArea = CheckInDtoObj.ServiceArea;
        //       string subServiceArea = CheckInDtoObj.SubServiceArea;
        //       string insightValue = CheckInDtoObj.InsightValue;
        //        bool boxExists = CheckInDtoObj.BoxExists;
        //        int numofBatches = CheckInDtoObj.NumofBatches;
        //        bool available = true;

        //        string insertStatement = "INSERT INTO Checkin_Tbl (ItemCode, Financecode,ServiceArea,SubServiceArea,InsightValue,BoxExists,Available,NumofBatches) " +
        //                                 "VALUES (@itemCode, @financecode,@serviceArea,@subServiceArea,@insightValue,@boxExists,@available,@numofBatches)";
        //        using (SqlCommand command = new SqlCommand(insertStatement, connection))
        //        {
        //            command.Parameters.AddWithValue("@ItemCode", itemCode);
        //            command.Parameters.AddWithValue("@Financecode", financecode);
        //            command.Parameters.AddWithValue("@ServiceArea", serviceArea);
        //            command.Parameters.AddWithValue("@SubServiceArea", subServiceArea);
        //            command.Parameters.AddWithValue("@InsightValue", insightValue);
        //            command.Parameters.AddWithValue("@boxExists", boxExists);
        //            command.Parameters.AddWithValue("@available", available);
        //            command.Parameters.AddWithValue("@numofBatches", numofBatches);

        //            int rowsAffected = command.ExecuteNonQuery();

        //            if (rowsAffected > 0)
        //            {
        //                Console.WriteLine("Data inserted successfully.");
        //                response = "Data inserted successfully";
        //            }
        //            else
        //            {
        //                Console.WriteLine("No rows were affected.");
        //                response = "Failed";
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("Error: " + ex.Message);
        //    }
        //}

        public HashSet<string> GetDocTypes()
        {
            HashSet<string> docTypes = new HashSet<string>();
            string query = "select distinct InsightValue from MasterInventory";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                           
                            if (reader.HasRows)
                            {

                                while (reader.Read())
                                {
                                    string value = reader.GetString(0);
                                    docTypes.Add(value);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error: " + ex.Message);
                    }
                }
            }
            return docTypes;
        }

        public string SaveTids(string boxId ,List<TID_Dto> tidDtoObj)
        {
            int checkinTblId = 0;
            var query = new Sql()
                        .Select("*")
                        .From("dbo.Checkin_Tbl")
                        .Where("ItemCode = @0", boxId);

            var checkinTbldata = this.Context.SingleOrDefault<CheckinTbl>(query);
            if (checkinTbldata != null)
            {
                checkinTblId = checkinTbldata.Id;
            }
            string response = string.Empty;
            List<TID_Tbl> tids_List = new List<TID_Tbl>();
            if(checkinTblId > 0)
            {
                foreach (var tidObj in tidDtoObj)
                {
                    TID_Tbl tids = new TID_Tbl();
                    tids.Checkin_Id = checkinTblId;
                    tids.TID = tidObj.TID;
                    tids.User_ID = 1;
                    tids.Available = true;
                    // tids_List.Add(tids);
                    var res = this.Context.Insert(tids);
                    response = "Tid details inserted successfully.";
                }
            }          

            //if (tids_List.Count > 0)
            //{
            //    var res = this.Context.ExecuteInsert("TID_Tbl","Id",true,tids_List);                
            //    response = "Tid details inserted successfully.";
            //}
            return response;
        }

        //public List<TID_Tbl> GetTidsList(string boxId)
        //{
        //    TID_Tbl tid = new TID_Tbl();
        //    List<TID_Tbl> tidList = new List<TID_Tbl>();
        //    var query = new Sql()
        //    .Select("Id")
        //    .From("dbo.TID_Tbl")
        //    .Where("ItemCode = @0", boxId);

        //    tid = this.Context.<TID_Tbl>(query);
            
        //    tidList.Add(tid);
        //    return tidList;
        //}

        public long GetNewTid()
        {
            TID_Tbl tid = new TID_Tbl();
            long id = 0;
            var query = new Sql()
            .Select("top 1 TID")
            .From("dbo.TID_Tbl").OrderBy("TID DESC");

            tid = this.Context.SingleOrDefault<TID_Tbl>(query);
            if(tid == null)
            {
                id = 10000001;
            }
            else
            {
                id = tid.TID + 1;
            }
            
            return id;
        }
        public string SaveTidsList(string boxId, string userName, string skpbox)
        {
            long newtid = GetNewTid();
            string response = string.Empty;
            long user_Id = this.GetUserId(userName);
            List<TID_Tbl> tids_List = new List<TID_Tbl>();
            TID_Tbl tids = new TID_Tbl();
            tids.TID = newtid;
            tids.ItemCode = boxId;
            tids.User_ID = user_Id;
            tids.Available = true;
            tids.IsDeleted = false; 
            
            var res = this.Context.Insert(tids);
            response = tids.TID.ToString();
            if (response != string.Empty)
            {
                this.AddE2EAuditTempTable(tids, skpbox);
            }
            return response;
        }

        public HashSet<long> GetTidsList(string boxId)
        {
            HashSet<long> tidLists = new HashSet<long>();
            string query = "select TID  from TID_Tbl where ItemCode = @itemCode And IsDeleted=0 And Available =1";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                        command.Parameters.AddWithValue("@itemCode", boxId);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                                while (reader.Read())
                                {
                                var x = reader.GetInt64(0);
                                    //var value = reader.GetString(0);
                                    tidLists.Add(x);
                                }
                            
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error: " + ex.Message);
                    }
                }
            }
            return tidLists;
        }

        public string DeleteTid(long tid)
        {
            string response = string.Empty;
            string query = "UPDATE TID_Tbl SET IsDeleted = @value WHERE TID = @tid";
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@value", 1);
                        command.Parameters.AddWithValue("@tid", tid);

                        int rowsAffected = command.ExecuteNonQuery();

                    }
                }
                response = "Tid deleted successfully.";
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred: " + ex.Message);
            }
            return response;
        }

        private int? UpdateTidwithCheckin(string boxId)
        {
            string response = string.Empty;
            var res = this.GetBoxDetails(boxId, "box");
            int? checkinId = res.Id;
            string query = "UPDATE TID_Tbl SET Checkin_Id = @checkinId WHERE ItemCode = @boxId";
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@checkinId", checkinId);
                        command.Parameters.AddWithValue("@boxId", boxId);

                        int rowsAffected = command.ExecuteNonQuery();

                    }
                }
                response = "Tid deleted successfully.";
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred: " + ex.Message);
            }
            return checkinId;
        }

        private long GetUserId(string user_name)
        {
            User userIds = new User();
            long userId =0;
            var query = new Sql()
            .Select("top 1 UserID")
            .From("dbo.Users")
            .Where("UserName =@0", user_name);

            userIds = this.Context.SingleOrDefault<User>(query);
            userId = userIds.UserID;
            return userId;
        }

        public void CreateBatch(string BoxID, int batchesCount, string skpBox, long user_Id)
        {
            try
            {
                var res = this.GetBoxDetails(BoxID, "box");
                var checkinId = res.Id;
                int bTypeConverted = batchesCount;
                var response = new object();
                List<Batch_Label_TBL> batchLabels = new List<Batch_Label_TBL>();
                for (int i = 1; i <= batchesCount; i++)
                {
                    int consecutive = 0;
                    consecutive = GetLastConsecutiveByBoxID(checkinId);

                    consecutive++;

                    //Set the name of the batch label
                    string batchName = (skpBox + "_" + consecutive.ToString() + "_of_" + bTypeConverted).ToUpper().Trim();
                    var batch = new Batch_Label_TBL()
                    {
                        CheckinID = checkinId,
                        Batch_Name = batchName,
                        User_ID = user_Id,
                        Batch_Consecutive_Number = consecutive,
                        Available = true
                    };

                    AddBatchLabel(batch);
                }


            }

            catch (Exception e)
            {
                Utils.Debug.Trace($"Exception. {e.Message} {e.StackTrace}");

            }
        }

        public void AddBatchLabel(Batch_Label_TBL BatchLabel)
        {
            this.Context.Insert(BatchLabel);
        }

        public int GetLastConsecutiveByBoxID(int CheckinID)
        {
            Sql sql = new Sql();
            sql.Select("ISNULL(MAX([Batch_Consecutive_Number]), 0 )")
            .From("[Batch_Label_TBL]")
            .Where("[CheckinID] = @0 ", CheckinID);

            var cnum = this.Context.ExecuteScalar<int>(sql);
            return cnum;
        }

        public void AddE2EAuditTempTable(TID_Tbl tidDetails, string skpbox)
        {
            E2EAuditDto e2EAuditDtoObj = new E2EAuditDto();
            try
            {
                Sql sql = new Sql(@"INSERT INTO[E2E_Lambeth_Data].[dbo].[IMTrack_TAudit_Working](

                                    [Customer_ID]
                                    , [Project_ID]
                                    , [Task]
                                    , [Package_Name]
                                    , [Unit_Type]
                                    , [Total_Units]
                                    , [StartDateTime]
                                    ,[EndDateTime]
                                    , [UserID]
                                    , [SiteName]
                                    , [LogDateTime],[CheckinId])
                                      VALUES(@0, @1, @2, @3, @4, @5, @6, @7, @8, @9,@10,@11)",
                                      "Lambeth", "Lambeth", "CheckIn", skpbox, "TID",
                                      1, DateTime.Now, DateTime.Now, tidDetails.User_ID, "LUTT", DateTime.Now,0);
                var cnum = this.Context.ExecuteScalar<int>(sql);
            }
            catch(Exception ex)
            {

            }
        }

       

        private void E2E_Update_IMTrack_TAudit1(string boxId)
        {
            Sql getAuditcount = new Sql(@"Select count(*) from [E2E_Lambeth_Data].[dbo].[IMTrack_TAudit_Working]");
            int count = this.Context.ExecuteScalar<int>(getAuditcount);

            if (count > 0)
            {
                var query = new Sql()
                             .Select("top 1 EndDateTime")
                             .From(" [E2E_Lambeth_Data].[dbo].[IMTrack_TAudit_Working]")
                            .OrderBy("Taudit_id DESC");

                var lastDateTime = this.Context.SingleOrDefault<DateTime>(query);

                var totalUnitsQuery = new Sql()
                             .Select("count(*)")
                             .From(" [E2E_Lambeth_Data].[dbo].[IMTrack_TAudit_Working]");

                var totalUnits = this.Context.ExecuteScalar<int>(totalUnitsQuery);

                Sql auditWorkingDataQuery = new Sql(@" SELECT
                                    [Customer_ID],
                                    [Project_ID],
                                    [Task],
                                    [Package_Name],
                                    [Unit_Type],
                                    [Total_Units],
                                    [StartDateTime],
                                    [EndDateTime],
                                    [UserID],
                                    [SiteName],
                                    [LogDateTime],[CheckinId] FROM [E2E_Lambeth_Data].[dbo].[IMTrack_TAudit_Working]");

                var auditWorkingRecords = this.Context.FirstOrDefault<E2EAuditDto>(auditWorkingDataQuery);
                if (auditWorkingRecords != null)
                {
                        Sql sqlUpdate = new Sql(@"INSERT INTO [E2E_Lambeth_Data].[dbo].[IMTrack_TAudit]
                                          ([Customer_ID],[Project_ID],[Task],[Package_Name],[Unit_Type],[Total_Units],[StartDateTime],[EndDateTime],[UserID],[SiteName],[LogDateTime],[CheckinId])
                                          VALUES
                                          (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10,@11)",
                                            auditWorkingRecords.Customer_ID, auditWorkingRecords.Project_ID, auditWorkingRecords.Task,
                                            auditWorkingRecords.Package_Name, auditWorkingRecords.Unit_Type, totalUnits,
                                            auditWorkingRecords.StartDateTime, lastDateTime, auditWorkingRecords.UserID,
                                            auditWorkingRecords.SiteName, DateTime.Now, auditWorkingRecords.CheckinId);

                        this.Context.Execute(sqlUpdate);
                    

                    Sql sqlDelete = new Sql(@"DELETE FROM [E2E_Lambeth_Data].[dbo].[IMTrack_TAudit_Working]");

                    this.Context.Execute(sqlDelete);
                }
            }
        }

        private void E2E_Update_IMTrack_TAudit_Working(int? checkinId)
        {
            var updateCheckinId ="UPDATE[E2E_Lambeth_Data].[dbo].[IMTrack_TAudit_Working] SET CheckinId = @checkinId";
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(updateCheckinId, connection))
                    {
                        command.Parameters.AddWithValue("@checkinId", checkinId);
                        int rowsAffected = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred: " + ex.Message);
            }
        }


        private void DeleteBatchLable(int checkinId)
        {
            string deleteQuery = "DELETE FROM [dbo].[Batch_Label_TBL] WHERE [CheckinID] =@Value";

            int conditionValue = checkinId;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(deleteQuery, connection))
                {
                    command.Parameters.AddWithValue("@Value", conditionValue);
                    try
                    {
                        int rowsAffected = command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                    }
                }
            }
        }

        public string SaveHoldBoxDetails(CheckInDto CheckInDtoObj)
        {
            string response = string.Empty;
            long user_Id = this.GetUserId(CheckInDtoObj.UserName);
            try
            {
                CheckinTbl CheckinTblObj = new CheckinTbl();
                CheckinTblObj.ItemCode = CheckInDtoObj.ItemCode;
                CheckinTblObj.Financecode = CheckInDtoObj.Financecode;
                CheckinTblObj.ServiceArea = CheckInDtoObj.ServiceArea;
                CheckinTblObj.SubServiceArea = CheckInDtoObj.SubServiceArea;
                CheckinTblObj.InsightValue = CheckInDtoObj.InsightValue;
                CheckinTblObj.BoxExists = CheckInDtoObj.BoxExists;
                CheckinTblObj.NumofBatches = CheckInDtoObj.NumofBatches;
                CheckinTblObj.Available = true;
                CheckinTblObj.SKPBox = CheckInDtoObj.SKPBox;
                CheckinTblObj.User_ID = user_Id;
                CheckinTblObj.DateTime = DateTime.Now;
                if (CheckInDtoObj.Id > 0)
                {
                    CheckinTblObj.Id = CheckInDtoObj.Id;
                    CheckinTblObj.HoldBox = true;
                    this.Context.Update(CheckinTblObj);
                    response = "True";
                }
                else
                {
                    var x = this.Context.Insert(CheckinTblObj);
                    response = "True";
                }
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }


            return response;
        }

        public string CheckItemCodeFromExcelData(string boxCode)
        {
            BarcodeLookup barcodeLookupObj = new BarcodeLookup();
            var query = new Sql()
                .Select("*")
                .From("dbo.BarcodeLookup")
                .Where("Barcode = @0", boxCode);
            barcodeLookupObj = this.Context.SingleOrDefault<BarcodeLookup>(query);
            if(barcodeLookupObj != null)
            {
                var barcodeRetMsg = "Box Id is available in Adults files for destruction.";
                return barcodeRetMsg;
            }
                        
            return null;
        }

        public HashSet<string> GetBoxIdList(string tid)
        {
            HashSet<string> tidLists = new HashSet<string>();
            string query = "select ItemCode  from TID_Tbl where TID = @tid And IsDeleted=0 And Available =1";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                        command.Parameters.AddWithValue("@tid", tid);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                var x = reader.GetString(0);
                                //var value = reader.GetString(0);
                                tidLists.Add(x);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error: " + ex.Message);
                    }
                }
            }
            return tidLists;
        }

        private List<string> GetBoxIdListsMaster(string itemCode)
        {
            List<string> itemCodeLists = new List<string>();
            string query = "select REPLACE(ItemCode, '~', '')  from dbo.MasterInventory  where REPLACE(ItemCode, '~', '')  LIKE '%" + itemCode + "'";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                        command.Parameters.AddWithValue("@itemCode", itemCode);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                var x = reader.GetString(0);
                                itemCodeLists.Add(x);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error: " + ex.Message);
                    }
                }
            }
            return itemCodeLists;
        }

        private List<string> GetBoxIdListsCheckin(string itemCode)
        {
            List<string> itemCodeLists = new List<string>();
            string query = "select ItemCode  from dbo.Checkin_Tbl  where REPLACE(ItemCode, '~', '')  LIKE '%" + itemCode + "'";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                        command.Parameters.AddWithValue("@itemCode", itemCode);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                var x = reader.GetString(0);
                                itemCodeLists.Add(x);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error: " + ex.Message);
                    }
                }
            }
            return itemCodeLists;
        }

    }
}
