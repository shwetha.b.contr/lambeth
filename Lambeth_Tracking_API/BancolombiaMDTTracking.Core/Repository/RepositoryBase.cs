﻿using BancolombiaMDTTracking.Core.Data;
using BancolombiaMDTTracking.Core.Factories;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancolombiaMDTTracking.Core.Repository
{
    public interface IRepositoryBase<TEntity>
    {
        IEnumerable<TEntity> GetAll(string TableName);
        List<TEntity> GetByFilter(Sql sql);
        List<TKey> GetLisCustom<TKey>(Sql sql);
        TEntity Get<TKey>(TKey id);
        TEntity Get(Sql sql);
        TKey Get<TKey>(Sql sql);
        TKey Exist<TKey>(string TableName, string param, string value);
        TKey Exist<TKey>(Sql sql);
        TKey GetSingle<TKey>(Sql sql);
        TKey Add<TKey>(TEntity entity);
        void Modify(TEntity entity);
        void Remove(TEntity entity);
        TKey InsertOrUpdate<TKey>(TEntity entity);
        void BeginTransaction();
        void CommitTransaction();
        void RollBackTransaction();
        string SecuenceNumberBatchID();
    }

    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity>
    {
        public string Bancolombia_DataFeed = string.Empty;
        public string webstatusImTrack = string.Empty;
        public string E2EDatabase = String.Empty;
        public int E2EElapsedHours = 0;
        public string E2ECuromerID = string.Empty;
        public string E2EProjectID = string.Empty;
        protected DataContext _context;
        public DataContext Context { get { return _context ?? (_context = DbFactory.Init()); } }

        protected IDbFactory DbFactory { get; set; }

        public RepositoryBase(IDbFactory dbFactory)
        {
            DbFactory = dbFactory;
            Bancolombia_DataFeed = ConfigurationManager.AppSettings["DataFeed_Bancolombia"];
            webstatusImTrack = ConfigurationManager.AppSettings["Webstatus_Bancolombia"];
            E2EDatabase = ConfigurationManager.AppSettings["E2E_Bancolombia_Data"];
            E2EElapsedHours = Int32.Parse(ConfigurationManager.AppSettings["E2E_Elapsed_Hours"]);
            E2ECuromerID = ConfigurationManager.AppSettings["E2E_ProjectID"];
            E2EProjectID = ConfigurationManager.AppSettings["E2E_CustomerID"];
        }

        public TKey Add<TKey>(TEntity entity)
        {
            return (TKey)Context.Insert(entity);
        }

        public TEntity Get<TKey>(TKey id)
        {
            return Context.SingleOrDefault<TEntity>(id);
        }

        public TEntity Get(Sql sql)
        {
            return Context.SingleOrDefault<TEntity>(sql);
        }

        public TKey Get<TKey>(Sql sql)
        {
            return Context.SingleOrDefault<TKey>(sql);
        }

        public TKey Exist<TKey>(string TableName, string param, string value)
        {
            var sql = string.Format("SELECT count(1) FROM {0} where {1} = '{2}'", TableName, param, value);
            return Context.SingleOrDefault<TKey>(sql);
        }

        public TKey Exist<TKey>(Sql sql)
        {
            return Context.SingleOrDefault<TKey>(sql);
        }

        public TKey GetSingle<TKey>(Sql sql)
        {
            return Context.SingleOrDefault<TKey>(sql);
        }

        public IEnumerable<TEntity> GetAll(string TableName)
        {
            var sql = string.Format("SELECT * FROM {0}", TableName);
            return Context.Query<TEntity>(sql);
        }


        public List<TEntity> GetByFilter(Sql sql)
        {
            return Context.Fetch<TEntity>(sql);
        }

        public List<TKey> GetLisCustom<TKey>(Sql sql)
        {
            return Context.Fetch<TKey>(sql);
        }

        public TKey InsertOrUpdate<TKey>(TEntity entity)
        {
            var pd = PocoData.ForType(typeof(TEntity), Context.DefaultMapper);
            var primaryKey = pd.TableInfo.PrimaryKey;

            var id = entity.GetType().GetProperty(primaryKey).GetValue(entity, null);
            var exists = Context.SingleOrDefault<TEntity>(id);

            if (!EqualityComparer<TEntity>.Default.Equals(exists, default(TEntity)))
            {
                Context.Update(entity);
                return (TKey)id;
            }

            return (TKey)Context.Insert(entity);
        }

        public void Modify(TEntity entity)
        {
            Context.Update(entity);
        }

        public void Remove(TEntity entity)
        {
            Context.Delete(entity);
        }

        public void BeginTransaction()
        {
            Context.BeginTransaction();
        }

        public void CommitTransaction()
        {
            Context.CompleteTransaction();
        }

        public void RollBackTransaction()
        {
            Context.AbortTransaction();
        }

        public string SecuenceNumberBatchID()
        {

            var sql = new Sql("SELECT NEXT VALUE FOR Sequence_VirtualBatch");


            var trackingIDNumber = this.Context.ExecuteScalar<long>(sql);

            var trackingIDNumberFormat = $"{trackingIDNumber:000000000}";

            return trackingIDNumberFormat;
        }
       
    }
}
