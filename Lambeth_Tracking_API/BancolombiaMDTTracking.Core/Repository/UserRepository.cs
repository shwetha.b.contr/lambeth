﻿using dbconnection;
using BancolombiaMDTTracking.Core.Factories;
using PetaPoco;
using System.Collections.Generic;

namespace BancolombiaMDTTracking.Core.Repository
{
    public interface IUsersRepository : IRepositoryBase<User>
    {

        List<dynamic> GetByDynamicFilter(Sql sql);
        User GetUser(string usr, string password);
        User GetUser(string usr);

    }

    public class UsersRepository : RepositoryBase<User>, IUsersRepository
    {
        public UsersRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public List<dynamic> GetByDynamicFilter(Sql sql)
        {
            return this.Context.Fetch<dynamic>(sql);
        }

        public User GetUser(string usr, string password)
        {
            var query = new Sql()
                .Select("*")
                .From("dbo.Users")
                .Where("lower(userName) = @0 and UserPassword = @1 AND Available = 1", usr.ToLower(), password);

            var user = this.Context.SingleOrDefault<User>(query);

            return user;
        }

        public User GetUser(string usr)
        {
            var query = new Sql()
                .Select("*")
                .From("dbo.Users")
                .Where("lower(UserName) = @0 AND Available = 1", usr.ToLower());

            var user = this.Context.SingleOrDefault<User>(query);

            return user;
        }
    }
}
