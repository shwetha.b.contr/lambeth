﻿using dbconnection;
using BancolombiaMDTTracking.Core.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancolombiaMDTTracking.Core.Repository
{
    public interface IUserTypeRepository : IRepositoryBase<UserType>
    {


    }

    public class UserTypeRepository : RepositoryBase<UserType>, IUserTypeRepository
    {

        public UserTypeRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }


    }
}
