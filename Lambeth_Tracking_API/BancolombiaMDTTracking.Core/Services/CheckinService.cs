﻿using BancolombiaMDTTracking.Core.Entities.Dto;
using BancolombiaMDTTracking.Core.Repository;
using dbconnection;
using LambethTracking.Core.Entities.Dto;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancolombiaMDTTracking.Core.Services
{
    public interface ICheckinService
    {
        ExistingBoxDetails GetBoxDetails(string boxCode, string type);
        string SaveBoxDetails(CheckInDto CheckInDtoObj);
        HashSet<string> GetDocTypes();
        string SaveTids(string boxId, List<TID_Dto> tidDtoObj);
        HashSet<long> GetTidsList(string boxId);
        string SaveTidsList(string boxId, string userName, string skpbox);
        string DeleteTid(long tid);
        string SaveHoldBoxDetails(CheckInDto CheckInDtoObj);
        string CheckItemCodeFromExcelData(string boxCode);
        HashSet<string> GetBoxIdList(string tid);
    }

    public class CheckinService : ICheckinService
    {
        private readonly ICheckInRepository _checkinRepository;
        public static string HoldBoxFilePath;
        public static string HoldBoxFolderPath;
        public static string HoldBoxDestFilePath;
        public static string TIDImagePath;
        public CheckinService(ICheckInRepository checkinRepository)
        {
            _checkinRepository = checkinRepository;
        }


        public ExistingBoxDetails GetBoxDetails(string boxCode, string type)
        {
            return _checkinRepository.GetBoxDetails( boxCode,  type);
        }

        public string SaveBoxDetails(CheckInDto CheckInDtoObj)
        {

            return _checkinRepository.SaveBoxDetails(CheckInDtoObj);
        }

        public HashSet<string> GetDocTypes()
        {
            HashSet<string> docTypes = new HashSet<string>();
            docTypes = _checkinRepository.GetDocTypes();
            return docTypes;
        }

        public string SaveTids(string boxId, List<TID_Dto> tidDtoObj)
        {
            return _checkinRepository.SaveTids(boxId,tidDtoObj);
        }

        public HashSet<long> GetTidsList(string boxId)
        {
            return _checkinRepository.GetTidsList(boxId);          
        }

        public string SaveTidsList(string boxId, string userName, string skpbox)
        {
            return _checkinRepository.SaveTidsList(boxId, userName, skpbox);
        }

        public string DeleteTid(long tid)
        {
            return _checkinRepository.DeleteTid(tid);
        }

        public string SaveHoldBoxDetails(CheckInDto CheckInDtoObj)
        {
            string message = string.Empty;
            var response = _checkinRepository.SaveHoldBoxDetails(CheckInDtoObj);
            if (response == "True")
            {
                this.GenerateTheHoldBoxTextFile(CheckInDtoObj);
                var destFolder =  this.CopyAllTheRequiredFilesIntoSingleFolder(CheckInDtoObj);
                var zipFilePath = this.GenerateTheZipFile(destFolder);
                this.CopyTheZipFileIntoGivenLocation(CheckInDtoObj, zipFilePath);
                message = CheckInDtoObj.ItemCode + ": Box has is in hold.";
                return message;
             }
            return null;
        }

        private string GenerateTheHoldBoxTextFile(CheckInDto CheckInDtoObj)
        {
            string fileName = "OUT_"+CheckInDtoObj.ItemCode+"_"+CheckInDtoObj.SKPBox+".txt";
            HoldBoxFilePath = ConfigurationManager.AppSettings["HoldBoxFilePath"];
            HashSet<long> boxTidList = _checkinRepository.GetTidsList(CheckInDtoObj.ItemCode);
            if (!Directory.Exists(HoldBoxFilePath))
            {
                Directory.CreateDirectory(HoldBoxFilePath);
            }
            string filePath = Path.Combine(HoldBoxFilePath, fileName);
            StringBuilder textFileData = new StringBuilder();
            //textFileData.AppendLine("Customer Box,  SKP Box, Finance Code, Subservice Area, Service Area, Doc Type, Reason Code, Description, TIDList");
            //textFileData.Append(CheckInDtoObj.ItemCode + "," + CheckInDtoObj.SKPBox + "," + CheckInDtoObj.Financecode + "," +
            //                        CheckInDtoObj.SubServiceArea + "," + CheckInDtoObj.ServiceArea + ","+ CheckInDtoObj.InsightValue + ","+
            //                        CheckInDtoObj.Reasoncode + ","+ CheckInDtoObj.Description + ",");
            //foreach (var tid in boxTidList)
            //{
            //    textFileData.Append(tid.ToString());
            //    textFileData.Append(",");
            //}
            textFileData.AppendLine("[Exception]");
            textFileData.AppendLine("Customer Box  = " + CheckInDtoObj.ItemCode);
            textFileData.AppendLine("SKP Box   =  " + CheckInDtoObj.SKPBox);
            textFileData.AppendLine("Finance Code   =  " + CheckInDtoObj.Financecode);
            textFileData.AppendLine("Subservice Area   =  " + CheckInDtoObj.SubServiceArea);
            textFileData.AppendLine("Service Area   = " + CheckInDtoObj.ServiceArea);
            textFileData.AppendLine("Doc Type   =  " + CheckInDtoObj.InsightValue);
            textFileData.AppendLine("Reason Code   = " + CheckInDtoObj.Reasoncode);
            textFileData.AppendLine("Description   = " + CheckInDtoObj.Description);

            textFileData.Append("TIDList = ");
            var tidcount = boxTidList.Count();
            var countinloop = 0;
            foreach (var tid in boxTidList)
            {
                countinloop++;
                if(tidcount == countinloop)
                {
                    textFileData.Append(tid.ToString());
                }
                else
                {
                    textFileData.Append(tid.ToString() + ",");
                }
               
            }
            try
            {
                System.IO.File.WriteAllText(filePath, textFileData.ToString());
                
            }
            catch (Exception ex)
            {
            }
            return filePath;
        }
        private string CopyAllTheRequiredFilesIntoSingleFolder(CheckInDto CheckInDtoObj)
        {
            HoldBoxFolderPath = ConfigurationManager.AppSettings["HoldBoxFolderPath"];
            HoldBoxFilePath = ConfigurationManager.AppSettings["HoldBoxFilePath"];
            TIDImagePath = ConfigurationManager.AppSettings["TIDImagePath"];
            var destFolder = HoldBoxFolderPath + "/"+ "OUT_"+CheckInDtoObj.ItemCode+"_"+CheckInDtoObj.SKPBox;
            if (!Directory.Exists(destFolder))
            {
                Directory.CreateDirectory(destFolder);
            }
            #region copy text file
            string[] files = Directory.GetFiles(HoldBoxFilePath);
            foreach (string txtfile in files)
            {
                string fileName = Path.GetFileName(txtfile);
                string destFile = Path.Combine(destFolder, fileName);
                File.Copy(txtfile, destFile, true);
                File.Delete(txtfile);
            }
            #endregion

            #region Copy tid images folders

            CopyAllFilesAndSubfolders(CheckInDtoObj, TIDImagePath, destFolder);

            #endregion
            return destFolder;
        }
        private string GenerateTheZipFile(string destFolder)
        {
            var holdBoxZipFilePath = destFolder + ".zip";           
            ZipFile.CreateFromDirectory(destFolder, holdBoxZipFilePath);
            Directory.Delete(destFolder, true);
            return holdBoxZipFilePath;
        }
        private void CopyTheZipFileIntoGivenLocation(CheckInDto CheckInDtoObj, string zipFilePath)
        {
            HoldBoxDestFilePath = ConfigurationManager.AppSettings["HoldBoxDestFilePath"];

            if (!Directory.Exists(HoldBoxDestFilePath))
            {
                Directory.CreateDirectory(HoldBoxDestFilePath);
            }
            string destinationZipFilePath = Path.Combine(HoldBoxDestFilePath, Path.GetFileName(zipFilePath));
            File.Copy(zipFilePath, destinationZipFilePath, true);
            File.Delete(zipFilePath);
        }
        
        private void CopyAllFilesAndSubfolders(CheckInDto CheckInDtoObj, string sourceDirectory, string destFolder)
        {
            HashSet<long> boxTidList = _checkinRepository.GetTidsList(CheckInDtoObj.ItemCode);
            foreach (string subdirectory in Directory.GetDirectories(TIDImagePath))
            {
                string subdirectoryName = Path.GetFileName(subdirectory);
                long tidFolderName = long.Parse(subdirectoryName);
                if (boxTidList.Contains(tidFolderName))
                {
                    string destinationSubdirectory = Path.Combine(destFolder, Path.GetFileName(subdirectory));
                    CopyAllFilesFromSubfolders(subdirectory, destinationSubdirectory);
                }

            }
        }

        private void CopyAllFilesFromSubfolders(string sourceDirectory, string destinationDirectory)
        {
            if (!Directory.Exists(destinationDirectory))
            {
                Directory.CreateDirectory(destinationDirectory);
            }
            foreach (string file in Directory.GetFiles(sourceDirectory))
            {
                string destinationFile = Path.Combine(destinationDirectory, Path.GetFileName(file));
                File.Copy(file, destinationFile, true);
            }
        }

        public string CheckItemCodeFromExcelData(string boxCode)
        {
            return _checkinRepository.CheckItemCodeFromExcelData(boxCode);
        }

        public HashSet<string> GetBoxIdList(string tid)
        {
            return _checkinRepository.GetBoxIdList(tid);
        }
    }

}
