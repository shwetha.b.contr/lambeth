﻿using BancolombiaMDTTracking.Core.Repository;
using dbconnection;

using PetaPoco;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancolombiaMDTTracking.Api.Controllers
{
    public interface IImage_SegmentService
    {
       // Images_Segment GetImagesSegment(long id);
       // Images_Segment GetImage(string image_name);
        //bool InsertUpdateImage();
        //bool UpdateImage( out string message);
      
        //long GetNextOrderId(long batchid, string segment);
    }
    public class Image_SegmentService: IImage_SegmentService
    {
       // private readonly IImage_SegmentRepository _Image_SegmentRepository;
       // private readonly IJobRepository _JobRepository;
        //public Image_SegmentService(IImage_SegmentRepository Image_SegmentRepository)
        //{
        //    _Image_SegmentRepository = Image_SegmentRepository;
        //   // _JobRepository = JobRepository;
        //}

        //public Images_Segment GetImagesSegment(long id)
        //{
        //    return _Image_SegmentRepository.Get(id);
        //}

        //public Images_Segment GetImage(string image_name)
        //{
        //    var query = new Sql();
        //    query.Select("*")
        //        .From("Images_Segments")
        //        .Where("Image_Name = @0", image_name);
        //    return _Image_SegmentRepository.Get(query);
        //}

        //public List<Images_Segment> GetImagesSegment_BeforePull(long batchid, string segment)
        //{
        //    var query = new Sql();
        //    query.Select("*")
        //        .From("Images_Segments")
        //        .Where("Batch_Id = @0 AND Segment = @1", batchid, segment);
        //    var result = _Image_SegmentRepository.GetByFilter(query);

        //    result.ForEach(x => {
        //        try
        //        {
        //            x.Image_Thumb_Base64 = "data:image/jpg;base64," +
        //            ReadConfig.get_Image_Base64(x.Image_Thumbnail, x.Batch_Id, x.Segment);
        //        }
        //        catch
        //        {
        //            x.Image_Thumb_Base64 = ReadConfig.ImageDumyThumb_Base64;
        //        }
        //    });
        //    return result;
        //}

        //public List<Images_Segment> GetImagesSegment(long batchid, string segment)
        //{
        //    var query = new Sql();
        //    query.Select("*")
        //        .From("Images_Segments")
        //        .Where("Batch_Id = @0 AND Segment = @1", batchid, segment);
        //    var result = _Image_SegmentRepository.GetByFilter(query);
        //    return result;
        //}

        //public bool InsertUpdateImage()
        //{
        //    message = string.Empty;
        //    bool result = false;
        //    try
        //    {
        //        if (Images_Segment == null)
        //        {
        //            message = "Invalid data, please call support.";
        //            return false;
        //        }
        //        //-----------------------increase Order_Id------------------------
        //        var query = new Sql();
        //        //query.Select("*")
        //        //    .From("Images_Segments")
        //        //    .Where("Batch_Id = @0 AND Segment = @1 AND Order_Id >= @2", Images_Segment.Batch_Id, Images_Segment.Segment, Images_Segment.Order_Id)
        //        //    .OrderBy("Order_Id");

        //        var lstImages = _Image_SegmentRepository.GetByFilter(query);
        //        for (int i = 0; i < lstImages.Count; i++)
        //        {
        //            if (lstImages[i].Order_Id != i + 1)
        //            {
        //                lstImages[i].Order_Id = lstImages[i].Order_Id + 1;
        //                _Image_SegmentRepository.InsertOrUpdate<long>(lstImages[i]);

        //            }
        //        }
        //        //---------------------------------------------------------------

        //        var ImageExist = _Image_SegmentRepository.Get(Images_Segment.Id);
        //        if (ImageExist == null)
        //        {
        //            Images_Segment.Creation_Date = DateTime.UtcNow;
        //        }    

        //        var id = _Image_SegmentRepository.InsertOrUpdate<long>(Images_Segment);
        //        //Images_Segment.Id = id;

        //        message = "The information was successfully saved.";
        //        result = true;
                
        //    }
        //    catch (Exception ex)
        //    {
        //        result = false;
        //        message = "Image could not be saved Error: " + ex.Message;
        //    }
        //    return result;
        //}

        //public bool UpdateImage(Images_Segment Images_Segment, out string message)
        //{
        //    message = string.Empty;
        //    bool result = false;
        //    try
        //    {
        //        if (Images_Segment == null)
        //        {
        //            message = "Invalid data, please call support.";
        //            return false;
        //        }

        //        var ImageExist = _Image_SegmentRepository.Get(Images_Segment.Id);
                
        //        if (ImageExist == null)
        //        {
        //            Images_Segment.Creation_Date = DateTime.UtcNow;
        //        }

        //        //Images_Segment.Image_Thumb_Base64 = null;

        //        _Image_SegmentRepository.Modify(Images_Segment);

        //        message = "The information was successfully saved.";
        //        result = true;

        //    }
        //    catch (Exception ex)
        //    {
        //        result = false;
        //        message = "Image could not be saved Error: " + ex.Message;
        //    }
        //    return result;
        //}

        //public bool DeleteImage(Images_Segment images_segment, string root, out string Message)
        //{
        //    Message = string.Empty;
        //    bool result = false;
        //    try
        //    {
        //        string pathThumb = ReadConfig.PhysicalPathThumbnails
        //            .Replace("[%BATCH_ID%]", images_segment.Batch_Id.ToString())
        //            .Replace("[%SEGMENT%]", images_segment.Segment);

        //        var _Image = _Image_SegmentRepository.Get(images_segment.Id);

        //        _Image_SegmentRepository.Remove(_Image);
        //        try
        //        {
        //            System.IO.File.Delete(root + _Image.Image_Name);
        //            System.IO.File.Delete(pathThumb + _Image.Image_Thumbnail);
        //        }
        //        catch (Exception)
        //        { }

        //        ////-----------------------reorder Order_Id------------------------
        //        //var query = new Sql();
        //        //query.Select("*")
        //        //    .From("Images_Segments")
        //        //    .Where("Batch_Id = @0 AND Segment = @1", _Image.Batch_Id, _Image.Segment)
        //        //    .OrderBy("Order_Id");

        //        //var lstImages = _Image_SegmentRepository.GetByFilter(query);
        //        //for(int i = 0; i < lstImages.Count; i++)
        //        //{
        //        //    if(lstImages[i].Order_Id != i + 1)
        //        //    {
        //        //        lstImages[i].Order_Id = i + 1;
        //        //        _Image_SegmentRepository.InsertOrUpdate<long>(lstImages[i]);
        //        //    }
        //        //}
        //        ////---------------------------------------------------------------
        //       // Message = "Image was deleted " + _Image.Image_Name + " successfully";
        //        result = true;
        //    }
        //    catch (FileNotFoundException ex)
        //    {
        //        Message = "Image was deleted successfully";
        //        result = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Message = "Image could not be deleted Error: " + ex.Message;
        //    }
        //    return result;
        //}

        //public long GetNextOrderId(long batchid, string segment)
        //{
        //    var query = new Sql();
        //    query.Select("ISNULL(MAX(Order_Id), 0) as Order_Id")
        //        .From("Images_Segments")
        //        .Where("Batch_Id = @0 AND Segment = @1", batchid, segment);
        //    var orderid = _Image_SegmentRepository.GetSingle<long>(query);
        //    return orderid + 1;
        //}

        //public Images_SegmentDto GetImagesSegment_BeforeScan(long batchid, long imgid, out string message_byimg)
        //{
        //    Images_SegmentDto images_SegmentDto = null;
        //    message_byimg = "";
        //    try
        //    {
        //        List<Images_Segment> Segments = _Image_SegmentRepository.GetImagesSegment(batchid);
        //        //Segments.ForEach(x => {
        //        //    if (x.Id == imgid)
        //        //    {
        //        //        x.Image_Thumb_Base64 = "data:image/jpg;base64," + ReadConfig.get_Image_Base64(x.Image_Thumbnail, x.Batch_Id, x.Segment);
        //        //    }
        //        //    else
        //        //    {
        //        //        x.Image_Thumb_Base64 = "";
        //        //    }
        //        //});

        //        List<dynamic> SegmentTotals = GetSegmentTotals(batchid);

        //        //images_SegmentDto = new Images_SegmentDto
        //        //{
        //        //    Segments = Segments,
        //        //    SegmentTotals = SegmentTotals
        //        //};

        //        return images_SegmentDto;
        //    }
        //    catch (Exception ex)
        //    {
        //        message_byimg = ex.Message;
        //        throw ex;
        //    }
        //}

        //public Images_SegmentDto GetImagesSegment_BeforeDelete(long batchid, long imgid)
        //{
        //    Images_SegmentDto images_SegmentDto = null;
        //    try
        //    {
        //        List<Images_Segment> Segments = _Image_SegmentRepository.GetImagesSegment(batchid);

        //        List<dynamic> SegmentTotals = GetSegmentTotals(batchid);

        //        //images_SegmentDto = new Images_SegmentDto
        //        //{
        //        //    Segments = Segments,
        //        //    SegmentTotals = SegmentTotals
        //        //};

        //        return images_SegmentDto;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        //private List<dynamic> GetSegmentTotals(long batchid)
        //{
        //    Sql query = new Sql();
        //    query.Select("segment, count(Id) as totalimages")
        //        .From("Images_Segments")
        //        .Where("Batch_Id = @0", batchid)
        //        .GroupBy("segment");

        //    List<dynamic> SegmentTotals = _Image_SegmentRepository.GetByDynamicFilter(query);
        //    return SegmentTotals;
        //}
    }
}
