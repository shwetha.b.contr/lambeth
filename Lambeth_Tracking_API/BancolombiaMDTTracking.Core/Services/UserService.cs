﻿using dbconnection;
using BancolombiaMDTTracking.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace BancolombiaMDTTracking.Core.Services
{
    public interface IUsersService
    {
        bool InsertUpdateUser(User User, out string Message);

        bool DeleteUser(long id, out string Message);

        User GetUser(long id);

        User GetUser(string usr, string password);

        List<User> GetUsers();
        User VerifyUserLogin(string usr, string password);
    }


    public class UsersService : IUsersService
    {
        private readonly IUsersRepository _usersRepository;

        public UsersService(IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository;
        }

        public bool DeleteUser(long id, out string Message)
        {
            Message = string.Empty;
            bool result = false;
            try
            {
                var User = _usersRepository.Get(id);

                _usersRepository.Remove(User);

                Message = string.Format("User deleted {0} successfully", User.UserID);
                result = true;
            }
            catch (Exception ex)
            {

                Message = string.Format("User could not be deleted Error: {0}", ex.Message);
            }
            return result;
        }

        public List<User> GetUsers()
        {
            return _usersRepository.GetAll("Users").ToList();
        }

        public User GetUser(string usr, string password)
        {
            return _usersRepository.GetUser(usr, password);
        }

        public User GetUser(long id)
        {

            var user = _usersRepository.Get(id);
            //user.op_password = CryptorEngine.Decrypt(user.op_password);
            return user;

        }

        public bool InsertUpdateUser(User User, out string Message)
        {
            Message = string.Empty;
            bool result = false;
            try
            {
                if (User == null)
                {
                    Message = "Invalid data, please call support.";
                    return false;
                }


                var user = _usersRepository.GetUser(User.UserName);
                if (user != null && User.UserID == 0)
                {
                    Message = "User '" + User.UserName + "' already exist.";
                    result = false;
                }
                else
                {

                    if (string.IsNullOrEmpty(User.UserPassword))
                    {
                        User.UserPassword = "123456";
                    }
                    User.UserPassword = HashPassword(User.UserPassword);
                    var len = User.UserPassword.Length;
                    User.UserCreated = DateTime.Now;
                    var id = _usersRepository.InsertOrUpdate<long>(User);


                    Message = "The information was successfully saved.";
                    result = true;
                }
            }
            catch (Exception ex)
            {
                result = false;
                Message = "User could not be saved Error: " + ex.Message;
            }

            return result;
        }
        public static string HashPassword(string password)
        {
            using (SHA256 sha256 = SHA256.Create())
            {
                byte[] hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(password));

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < hashedBytes.Length; i++)
                {
                    builder.Append(hashedBytes[i].ToString("x2")); 
                }
                return builder.ToString();
            }
        }

        
        public User VerifyUserLogin(string usr, string password)
        {
            var loginPassword = HashPassword(password);
            var savedPassword = string.Empty;
            var user = _usersRepository.GetUser(usr);
            if(user != null)
            {
                savedPassword = user.UserPassword;
            }
            if( loginPassword == savedPassword)
            {
                return user;
            }
            return null;
        }
    }
}
