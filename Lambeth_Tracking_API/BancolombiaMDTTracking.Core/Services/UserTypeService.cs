﻿using dbconnection;
using BancolombiaMDTTracking.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancolombiaMDTTracking.Core.Services
{
    public interface IUserTypeService
    {

        List<UserType> GetUserTypes();
        UserType GetUserType(long id);

    }


    public class UserTypeService : IUserTypeService
    {
        private readonly IUserTypeRepository _userTypeRepository;

        public UserTypeService(IUserTypeRepository userTypeRepository)
        {
            _userTypeRepository = userTypeRepository;
        }


        public List<UserType> GetUserTypes()
        {
            return _userTypeRepository.GetAll("tblUserTypes").ToList();
        }


        public UserType GetUserType(long id)
        {

            var user = _userTypeRepository.Get(id);
            return user;

        }


    }

}
