﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancolombiaMDTTracking.Core.Utils
{
    public class Debug
    {
        public static void Trace(string message)
        {
            if (ConfigurationManager.AppSettings["EnableLogging"] == "1")
            {
                System.Diagnostics.Trace.WriteLine($"{DateTime.Now:HHmmss.fff}: {message}");
            }
        }
    }
}
