﻿using System;
using System.Collections;
using System.IO;
using System.Windows.Forms;
using log4net;
using log4net.Config;

namespace IMShortProcess
{
    public sealed class ApLogsHandler
    {
        private static volatile ILog instance;

        private static object syncRoot;

        public static ILog Instance
        {
            get
            {
                if (ApLogsHandler.instance == null)
                {
                    lock (ApLogsHandler.syncRoot)
                    {
                        if (ApLogsHandler.instance == null)
                        {
                            GlobalContext.Properties["ApplicationLogsPath"] = Path.Combine(Application.StartupPath, "logs");
                            XmlConfigurator.ConfigureAndWatch(new FileInfo(string.Concat(Application.StartupPath, "\\IMShortProcessLogConfig.xml")));
                            ApLogsHandler.instance = LogManager.GetLogger("TextLogger");
                        }
                    }
                }
                return ApLogsHandler.instance;
            }
        }

        static ApLogsHandler()
        {
            ApLogsHandler.syncRoot = new object();
        }

        public ApLogsHandler()
        {
        }

        public static void LogError(object ex, ApLogsHandler.TypeError TypeE)
        {
            if (ApLogsHandler.Instance != null)
            {
                string str = string.Format("Exception: {0}\r\n", ex);
                if (TypeE.ToString().Equals("Error"))
                {
                    ApLogsHandler.Instance.Error(str.ToString());
                }
                if (TypeE.ToString().Equals("Info"))
                {
                    ApLogsHandler.Instance.Info(str.ToString());
                }
                if (TypeE.ToString().Equals("Warning"))
                {
                    ApLogsHandler.Instance.Warn(str.ToString());
                }
                if (TypeE.ToString().Equals("FatalError"))
                {
                    ApLogsHandler.Instance.Fatal(str.ToString());
                }
            }
        }

        public static void LogException(Exception ex, ApLogsHandler.TypeError TypeE)
        {
            if (ApLogsHandler.Instance != null)
            {
                string str = string.Format("Exception: {0}\r\n", ex.ToString());
                foreach (DictionaryEntry datum in ex.Data)
                {
                    str = string.Concat(str, string.Format("{0}: {1}\r\n", datum.Key, datum.Value));
                }
                if (TypeE.ToString().Equals("Error"))
                {
                    ApLogsHandler.Instance.Error(str.ToString());
                }
                if (TypeE.ToString().Equals("Info"))
                {
                    ApLogsHandler.Instance.Info(str.ToString());
                }
                if (TypeE.ToString().Equals("Warning"))
                {
                    ApLogsHandler.Instance.Warn(str.ToString());
                }
                if (TypeE.ToString().Equals("FatalError"))
                {
                    ApLogsHandler.Instance.Fatal(str.ToString());
                }
            }
        }

        public enum TypeError
        {
            Error,
            Info,
            Warning,
            FatalError
        }
    }
}
