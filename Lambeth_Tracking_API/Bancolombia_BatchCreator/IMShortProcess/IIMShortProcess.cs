﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMShortProcess
{
    public interface IIMShortProcess
    {
        dynamic Run(object JobConfig);

        event UpdateDataGridViewInProgressEventHandler TriggeredInProgress;
    }
}
