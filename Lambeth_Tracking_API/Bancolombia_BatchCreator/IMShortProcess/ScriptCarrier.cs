﻿using System.Xml;

namespace IMShortProcess
{
    public class ScriptCarrier
    {
        public XmlNode Configuration
        {
            get;
            set;
        }

        public IIMShortProcess ScriptAssemby
        {
            get;
            set;
        }

        public ScriptCarrier()
        {
        }
    }
}
