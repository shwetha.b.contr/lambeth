﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using CSScriptLibrary;
using Rlc.Cron;

namespace IMShortProcess
{

    public delegate bool UpdateDataGridViewBeforeEventHandler(object sender);
    public delegate void UpdateDataGridViewInProgressEventHandler(object sender, string msg, string status, int progress);
    public delegate void UpdateDataGridViewAfterEventHandler(object sender);

    public class JobConfigurationItem
    {
        #region Fields/Properties

        private List<string> _errors = null;
        private string _cron_expression = string.Empty;
        private bool _fixInitialRun = false;

        public XmlNode Configuration
        {
            get;
            set;
        }

        private CronObject cron
        {
            get;
            set;
        }

        public string CronExpression
        {
            get
            {
                return this._cron_expression;
            }
            set
            {
                this._cron_expression = value;
            }
        }

        public List<string> Errors
        {
            get
            {
                return this._errors;
            }
            private set
            {
                this._errors = value;
            }
        }

        public DateTime ExcecuteEnd
        {
            get;
            set;
        }

        public DateTime ExcecuteStart
        {
            get;
            set;
        }

        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string NameText
        {
            get;
            set;
        }

        public string ScriptFile
        {
            get;
            set;
        }

        public dynamic ScriptResult
        {
            get;
            set;
        }

        #endregion

        #region Constructor        
        public JobConfigurationItem() { }
        #endregion

        #region Functions

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cronObject"></param>
        public void Cron_OnCronTrigger(CronObject cronObject)
        {
            if (!this._fixInitialRun)
            {
                this._fixInitialRun = true;
            }
            else
            {
                this.ExcecuteStart = DateTime.Now;
                if (this.TriggeredBefore != null)
                {
                    this.TriggeredBefore(this);
                }
                ScriptCarrier scriptCarrier = (ScriptCarrier)cronObject.Object;
                try
                {

                    //UNCOMMENT THIS AFTER DEBUG !!!!  --> Run the Code from external file.                    
                    scriptCarrier.ScriptAssemby.TriggeredInProgress += TriggeredInProgress;
                    ScriptResult = scriptCarrier.ScriptAssemby.Run(this);

                    /*
                    // For Debug only --> Run the Code In the project, ScriptDebug.cs file.
                    //ScriptResult = new IMShortProcessScript().Run(scriptCarrier.Configuration);
                    IMShortProcessScript job2x = new IMShortProcessScript();
                    job2x.TriggeredInProgress += TriggeredInProgress;
                    ScriptResult = job2x.Run(this);
                     */
                }
                catch (Exception exception1)
                {
                    Exception exception = exception1;
                    this.ScriptResult = new List<string>()
                    {
                        exception.ToString()
                    };
                    //ApLogsHandler.LogException(exception, ApLogsHandler.TypeError.Error);
                }
                this.ExcecuteEnd = DateTime.Now;
                if (this.TriggeredAfter != null)
                {
                    this.TriggeredAfter(this);
                }
            }
        }

        /// <summary>
        /// Load Item Job from XML configuration file.
        /// </summary>
        /// <returns></returns>
        public bool Load()
        {
            // Item Validations
            this._errors = new List<string>();
            if (string.IsNullOrEmpty(this.ScriptFile))
            {
                this._errors.Add(string.Format("Script File is blank.", new object[0]));
            }
            else if (!File.Exists(this.ScriptFile))
            {
                this._errors.Add(string.Format("Script '{0}' doesn't exist.", this.ScriptFile));
            }
            if (string.IsNullOrEmpty(this.CronExpression))
            {
                this._errors.Add(string.Format("CronExpression expression is blank.", new object[0]));
            }

            // All good.
            if (this._errors.Count == 0)
            {
                try
                {
                    // Compile and create script.
                    CSScript.AssemblyResolvingEnabled = true;
                    AsmHelper asmHelper = new AsmHelper(CSScript.Load(this.ScriptFile, null, true, new string[0]));

                    // Create Scritp Interfqce.
                    IIMShortProcess iMShortProcess = (IIMShortProcess)asmHelper.CreateObject("IMShortProcess.IMShortProcessScript");

                    // Script configuration carrier.
                    ScriptCarrier scriptCarrier = new ScriptCarrier()
                    {
                        Configuration = this.Configuration,
                        ScriptAssemby = iMShortProcess
                    };

                    // Create Cron JOB Schedule from XML configuration. 
                    List<CronSchedule> cronSchedules = new List<CronSchedule>(){
                        CronSchedule.Parse(this._cron_expression)
                    };

                    // Cron Job
                    CronObjectDataContext cronObjectDataContext = new CronObjectDataContext()
                    {
                        Object = scriptCarrier,
                        CronSchedules = cronSchedules,
                        LastTrigger = DateTime.MinValue
                    };
                    this.cron = new CronObject(cronObjectDataContext);

                    // Add Trigger Event to Cron Job.
                    this.cron.OnCronTrigger += new CronObject.CronEvent(this.Cron_OnCronTrigger);
                }
                catch (Exception exception1)
                {
                    // Log Exception.
                    Exception exception = exception1;
                    //ApLogsHandler.LogException(exception, ApLogsHandler.TypeError.Error);
                    this._errors.Add(exception.Message);
                }
            }

            //
            return this._errors.Count == 0;
        }

        /// <summary>
        /// Start Item Job excecution.
        /// </summary>
        /// <returns></returns>
        public bool Start()
        {
            try
            {
                // Start Cron Job.
                this.cron.Start();
            }
            catch (Exception exception1)
            {
                Exception exception = exception1;
                this.Errors.Add(exception.Message);
                //ApLogsHandler.LogException(exception, ApLogsHandler.TypeError.Error);
                throw;
            }
            return this._errors.Count == 0;
        }

        /// <summary>
        /// Stop Item Job excecution.
        /// </summary>
        /// <returns></returns>
        public bool Stop()
        {
            try
            {
                this.cron.Stop();
            }
            catch (Exception exception1)
            {
                Exception exception = exception1;
                this.Errors.Add(exception.Message);
                //ApLogsHandler.LogException(exception, ApLogsHandler.TypeError.Error);
                throw;
            }
            return this._errors.Count == 0;
        }

        #endregion

        // Events for UI.
        public event UpdateDataGridViewAfterEventHandler TriggeredAfter;
        public event UpdateDataGridViewBeforeEventHandler TriggeredBefore;
        public event UpdateDataGridViewInProgressEventHandler TriggeredInProgress;
    }
}
