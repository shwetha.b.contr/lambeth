﻿using System;
using System.Xml;
using IMShortProcess;
using System.Collections.Generic;
using System.IO;

using System.Data.SqlClient;
using System.Data;

using System.Collections;
using System.Linq;
using System.Text;

using IMShortProcess.Class;
using System.Text.RegularExpressions;
using System.Globalization;


namespace IMShortProcess
{
    public class IMShortProcessScript : IIMShortProcess
    {
        /* GLOBALS */
        #region Global Variables
        string script_name_id = string.Empty;

        //
        private static ILogger _logText;
        public event UpdateDataGridViewInProgressEventHandler TriggeredInProgress;
        private enum TypeError { Error, Info, Warning, FatalError }
        #endregion

        /// <summary>
        /// Main Method ex by the Script Manager.
        /// </summary>
        /// <param name="JobConfig"></param>
        /// <returns></returns>
        public Object Run(Object JobConfig)
        {

            bool retval = true;

            // Cast configuration.
            JobConfigurationItem Job = JobConfig as JobConfigurationItem;
            XmlNode item = Job.Configuration;

            // Get configuration
            IMScriptUtils.LoadConfigItem(item);

            // If any Error Prevent EX.
            if (IMScriptUtils.Errors.Count > 0)
            {
                foreach (string err in IMScriptUtils.Errors)
                {
                    Error(err, LogMessageTypeEnum.Error);
                }
                return IMScriptUtils.Errors;
            }

            // Configuration [Required]
            script_name_id = IMScriptUtils.GetConfigParameter("script_name_id");
            string logs_path = IMScriptUtils.GetConfigParameter("logs_path");
            var file_tmp_ext = item.SelectSingleNode("file_tmp_ext").InnerText.Trim();
            var file_final_ext = item.SelectSingleNode("file_final_ext").InnerText.Trim();
            var output_directory = item.SelectSingleNode("output_directory").InnerText.Trim();
            var temporal_tid_image = item.SelectSingleNode("temporal_tid_image").InnerText.Trim();
            var envelopes_images_source = item.SelectSingleNode("envelopes_images_source").InnerText.Trim();
            var envelopes_images_target = item.SelectSingleNode("envelopes_images_target").InnerText.Trim();

            // Logs Setup            
            _logText = new FileLogger(logs_path, script_name_id, "log", true);

            // Database Server Info
            XmlNode nNodeDatabase = item.SelectSingleNode("database");

            string g_db_type = IMScriptUtils.GetConfigString(nNodeDatabase, "db_type");
            string g_db_host = IMScriptUtils.GetConfigString(nNodeDatabase, "db_host");
            string g_db_user = IMScriptUtils.GetConfigString(nNodeDatabase, "db_user");
            string g_db_password = IMScriptUtils.GetConfigString(nNodeDatabase, "db_password");
            string g_db_database = IMScriptUtils.GetConfigString(nNodeDatabase, "db_database");

            // Get files and order them by Modification Date.
            TriggeredInProgress(JobConfig, "Starting process", "In Progress", 0);
            _logText.LogMessage("Starting process", LogMessageTypeEnum.Info);

            try
            {
                // Create output directory
                if (!Directory.Exists(output_directory))
                {
                    Directory.CreateDirectory(output_directory);
                }

                //Query
                _logText.LogMessage("Connect to Database", LogMessageTypeEnum.Info);
                string ConnectionString = string.Format("Server={0};Database={1};Uid={2};Pwd={3};Connect Timeout=1800", g_db_host, g_db_database, g_db_user, g_db_password);
                SqlConnection dbCon = new SqlConnection(ConnectionString);

                // Excute query
                _logText.LogMessage("Open Connect to Database", LogMessageTypeEnum.Info);
                if (OpenDbConn(ref dbCon))
                {
                    // Read Query 
                    string query = @"SELECT
                                      SUBSTRING(B.[BatchName], 0, 11) as BatchName,
									  B.SiteName
                                    FROM [Bancolombia_MDT_Tracking].[dbo].[Batches] B
                                    WHERE [KofaxReady] = 'RDY'
                                    AND Available = 1
                                    GROUP BY SUBSTRING(B.[BatchName], 0, 11), B.[SiteName]";

                    DataTable dt = ExecuteDataTable(query, dbCon, null);

                    if (dt.Rows.Count > 0)
                    {
                        _logText.LogMessage(dt.Rows.Count.ToString() + " records for create xml batch", LogMessageTypeEnum.Info);

                        foreach (DataRow row in dt.Rows)
                        {
                            string tmpXmlFilePath = Path.Combine(output_directory, row["BatchName"].ToString() + file_tmp_ext);
                            string finalXmlFilePath = Path.Combine(output_directory, row["BatchName"].ToString() + file_final_ext);

                            try
                            {

                                #region XML CREATOR
                                // Create XML Files
                                // Create an XmlWriterSettings object with the correct options.
                                XmlWriterSettings settings = new XmlWriterSettings
                                {
                                    Indent = true,
                                    IndentChars = ("\t"),
                                    OmitXmlDeclaration = false
                                };

                                // Create the XmlWriter object and write some content.

                                

                                using (XmlWriter writer = XmlWriter.Create(tmpXmlFilePath, settings))
                                {
                                    writer.WriteStartElement("ImportSession");
                                    writer.WriteStartElement("Batches");
                                    writer.WriteStartElement("Batch");
                                    writer.WriteAttributeString("Name", null, row["BatchName"].ToString());
                                    writer.WriteAttributeString("Description", null, "");
                                    writer.WriteAttributeString("Priority", null, "5");
                                    writer.WriteAttributeString("BatchClassName", null, "BANCOLOMBIA_AUTO_" + row["SiteName"] + "_DCS");
                                   

                                    //writer.WriteStartElement("Documents"); //Documents
                                    //writer.WriteStartElement("Document"); //Document
                                    //writer.WriteAttributeString("FormTypeName", null, "BANCOLOMBIA_AUTO_BOGO_TIF");
                                    writer.WriteEndElement();  //Document

                                    writer.Flush();
                                }

                                #endregion

                                #region UPDATE DATA
                                // Update 2nd Webstatus batch_custom_data table
                                string updateBCD = @"UPDATE [Bancolombia_MDT_Tracking].[dbo].[Batches]
                                                        SET [KofaxReady] = 'CMP'
                                                    WHERE [BatchName] LIKE '" + row["BatchName"] + "%' AND Available = 1";

                                _logText.LogMessage("Start transaction.", LogMessageTypeEnum.Info);
                                SqlTransaction myTrans = dbCon.BeginTransaction();

                                try
                                {
                                    SqlCommand cmdUpdateBCD = new SqlCommand(updateBCD, dbCon, myTrans);
                                    int tmpUpdatedRows = cmdUpdateBCD.ExecuteNonQuery();
                                    _logText.LogMessage("Update " + tmpUpdatedRows.ToString() + " records on [Bancolombia_MDT_Tracking].[dbo].[Batches] for batch " + row["BatchName"].ToString(), LogMessageTypeEnum.Info);

                                    myTrans.Commit();
                                    _logText.LogMessage("Transaction Commited ..", LogMessageTypeEnum.Info);

                                    File.Move(tmpXmlFilePath, finalXmlFilePath);
                                    _logText.LogMessage("Renamed temporal file " + tmpXmlFilePath + " to final " + finalXmlFilePath, LogMessageTypeEnum.Info);

                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine("Commit Exception Type: {0}", ex.GetType());
                                    Console.WriteLine("  Message: {0}", ex.Message);

                                    // Attempt to roll back the transaction.
                                    try
                                    {
                                        _logText.LogMessage("Attempt to roll back the transaction.", LogMessageTypeEnum.Info);
                                        myTrans.Rollback();
                                        _logText.LogMessage("roll back completed.", LogMessageTypeEnum.Info);
                                    }
                                    catch (Exception ex2)
                                    {
                                        // This catch block will handle any errors that may have occurred
                                        // on the server that would cause the rollback to fail, such as
                                        // a closed connection.
                                        Console.WriteLine("Rollback Exception Type: {0}", ex2.GetType());
                                        Console.WriteLine("  Message: {0}", ex2.Message);
                                    }
                                }
                                #endregion
                            }
                            catch (Exception ex)
                            {
                                _logText.LogMessage("Can't create xml file " + finalXmlFilePath, LogMessageTypeEnum.Info);
                                _logText.LogMessage(ex.Message + Environment.NewLine + ex.StackTrace, LogMessageTypeEnum.Info);
                            }
                        }
                    }
                    else
                        _logText.LogMessage(" No records found for create xml batch", LogMessageTypeEnum.Info);
                }
            }
            catch (Exception exc)
            {
                _logText.LogMessage(exc.Message + Environment.NewLine + exc.StackTrace, LogMessageTypeEnum.Info);
            }

            //copy envelopes, rename tif to tiff in source path
            try
            {
                string[] files = Directory.GetFiles(envelopes_images_source, "*.tiff", SearchOption.AllDirectories);

                _logText.LogMessage(files.Count() + " files envelopes found for rename and move", LogMessageTypeEnum.Info);

                for (int i = 0; i < files.Length; i++)
                {
                    var dirName = new DirectoryInfo(files[i]).Parent.ToString();
                    var fileNameWithOutExt = Path.GetFileNameWithoutExtension(files[i]);
                    if (!Directory.Exists(Path.Combine(envelopes_images_target, dirName)))
                    {
                        Directory.CreateDirectory(Path.Combine(envelopes_images_target, dirName));
                    }
                    File.Copy(files[i], Path.Combine(envelopes_images_target, dirName) + @"\" + fileNameWithOutExt + ".tiff");
                    _logText.LogMessage(files[i] + " copied to " + Path.Combine(envelopes_images_target, dirName), LogMessageTypeEnum.Info);


                    File.Move(files[i], Path.Combine(envelopes_images_source, dirName) + @"\" + fileNameWithOutExt + ".tif");
                    _logText.LogMessage(files[i] + " renamed", LogMessageTypeEnum.Info);
                }
                _logText.LogMessage(files.Count() + " files envelopes was renamed and moved correctly", LogMessageTypeEnum.Info);
            }
            catch (IOException ioex)
            {
                _logText.LogMessage(ioex.Message + Environment.NewLine + ioex.StackTrace, LogMessageTypeEnum.Info);
            }
            catch (Exception ex)
            {
                _logText.LogMessage(ex.Message + Environment.NewLine + ex.StackTrace, LogMessageTypeEnum.Info);
            }
            //-----------------------------------------------------------------------------


            _logText.LogMessage("Process completed.", LogMessageTypeEnum.Info);
            TriggeredInProgress(JobConfig, "Process completed.", "Completed", 100);

            return retval;

        }

        private enum ErrorTypeEnum
        {
            Warning,
            Fatal
        }

        private class ErrorItem : Exception
        {
            public ErrorTypeEnum Type { get; set; }
            public string Message { get; set; }
        }


        #region Utils        

        /* ERROR MANAGER */
        /// <summary>
        /// Add Log with Exception to ILogger.
        /// </summary>
        /// <param name="message"></param>
        private void Error(Exception ex, LogMessageTypeEnum TypeE, bool addToErrList = true)
        {

            StringBuilder formattedError = new StringBuilder();
            formattedError.AppendFormat("Exception: {0}\r\n", ex.ToString());

            foreach (DictionaryEntry de in ex.Data) formattedError.AppendFormat("{0}: {1}\r\n", de.Key, de.Value);

            _logText.LogMessage(formattedError.ToString(), TypeE);

            if (addToErrList) IMScriptUtils.Errors.Add(formattedError.ToString());

            formattedError = null;
        }

        /// <summary>
        /// Add Log with Message Error to Log4Net and Base Form
        /// </summary>
        /// <param name="message"></param>
        private void Error(object ex, LogMessageTypeEnum TypeE, bool addToErrList = true)
        {
            _logText.LogMessage(ex.ToString(), TypeE);
            if (addToErrList) IMScriptUtils.Errors.Add(ex.ToString());
        }

        #endregion

        #region DbUtils

        /// <summary>
        /// Open Database Connection 
        /// </summary>
        /// <param name="cnn"></param>
        private bool OpenDbConn(ref SqlConnection cnn)
        {
            bool retval = true;
            if (cnn != null)
            {
                if (cnn.State == System.Data.ConnectionState.Broken || cnn.State == System.Data.ConnectionState.Closed)
                {
                    try { cnn.Close(); }
                    catch { }

                    try
                    {
                        cnn.Open();
                        retval = true;
                    }
                    catch
                    {
                        // Add Error message.
                        retval = false;
                    }
                }
            }
            return retval;
        }

        /// <summary>
        /// Execute an Select and Return DataTable
        /// </summary>
        /// <param name="query"></param>
        /// <param name="myCon"></param>
        /// <returns></returns>
        private DataTable ExecuteDataTable(string query, IDbConnection myCon, IDbTransaction myTrans)
        {
            DataTable dt = new DataTable();
            using (var myCommand = myCon.CreateCommand())
            {
                myCommand.CommandTimeout = 1800;
                myCommand.CommandText = query;
                if (myTrans != null) myCommand.Transaction = myTrans;
                using (var myReader = myCommand.ExecuteReader())
                {
                    dt.Load(myReader);
                    myReader.Close();
                }
            }
            return dt;
        }

        private T ExecuteUniqueResult<T>(string query, IDbConnection myCon, IDbTransaction myTrans)
        {
            T obj;
            using (var myCommand = myCon.CreateCommand())
            {
                myCommand.CommandText = query;
                if (myTrans != null) myCommand.Transaction = myTrans;
                obj = (T)myCommand.ExecuteScalar();
            }
            return obj;
        }

        /// <summary>
        /// Execute query
        /// </summary>
        /// <param name="query"></param>
        /// <param name="myCon"></param>
        /// <returns></returns>
        private int ExecuteNonQuery(string query, IDbConnection myCon, IDbTransaction myTrans)
        {
            int affected = -1;
            using (var myCommand = myCon.CreateCommand())
            {
                myCommand.CommandText = query;
                if (myTrans != null) myCommand.Transaction = myTrans;
                affected = myCommand.ExecuteNonQuery();
            }
            return affected;
        }

        #endregion

        #region Logger

        private enum LogMessageTypeEnum
        {
            Info,
            Warn,
            Fatal,
            Error
        }

        //
        private interface ILogger
        {
            void LogMessage(string strMessage, LogMessageTypeEnum logType);
            void Info(string strMessage);
            void Warn(string strMessage);
            void Error(string strMessage);
            void Fatal(string strMessage);
        }

        private class FileLogger : ILogger
        {
            private string m_strFileName;
            private string m_strLogFilePath;
            private string m_strLogFile;
            private object m_sync = new object();

            public FileLogger(string strLogFilePath, string strLogFileName, string sLogExtFile, bool AutoCreateFolderIfNotExist = true)
            {
                if (string.IsNullOrEmpty(sLogExtFile.Trim('.'))) sLogExtFile = "log";
                if (string.IsNullOrEmpty(strLogFilePath)) throw new ArgumentNullException(strLogFilePath);
                if (string.IsNullOrEmpty(strLogFileName)) throw new ArgumentNullException(strLogFileName);
                if (!Directory.Exists(strLogFilePath))
                {
                    if (AutoCreateFolderIfNotExist)
                    {
                        Directory.CreateDirectory(strLogFilePath);
                    }
                    else
                    {
                        throw new DirectoryNotFoundException(string.Format("Directory '{0}' doesn't exist and AutoCreateFolderIfNotExist argument is not enabled.", Path.GetDirectoryName(strLogFilePath)));
                    }
                }

                string currDay = DateTime.Now.ToString("yyyyMMdd");
                string currMachine = System.Environment.MachineName;

                m_strLogFilePath = strLogFilePath;
                m_strFileName = string.Format("{0}_{1}_{2}.{3}", strLogFileName, currMachine, currDay, sLogExtFile.Trim('.'));
                m_strLogFile = Path.Combine(m_strLogFilePath, m_strFileName);
            }

            public void LogMessage(string strMessage, LogMessageTypeEnum logType = LogMessageTypeEnum.Info)
            {
                lock (m_sync)
                {
                    using (StreamWriter writer = new StreamWriter(m_strLogFile, true))
                    {
                        writer.WriteLine(string.Format("{0} - {1} - {2} ", logType.ToString(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), strMessage));
                    }
                }
            }

            //
            public void Info(string strMessage)
            {
                LogMessage(strMessage, LogMessageTypeEnum.Info);
            }

            public void Warn(string strMessage)
            {
                LogMessage(strMessage, LogMessageTypeEnum.Warn);
            }

            public void Error(string strMessage)
            {
                LogMessage(strMessage, LogMessageTypeEnum.Error);
            }

            public void Fatal(string strMessage)
            {
                LogMessage(strMessage, LogMessageTypeEnum.Fatal);
            }

            public void Exception(string strMessage)
            {
                LogMessage(strMessage, LogMessageTypeEnum.Fatal);
            }
        }

        #endregion

    }

    #region Utils

    public class Column
    {
        public string Name { get; set; }
        public string Display { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    static class IMScriptUtils
    {
        private static Regex rxParams = new Regex(@"{%\w+%}", RegexOptions.Compiled);
        private static List<string> _errors = new List<string>();
        public static List<string> Errors { get { return _errors; } }

        private static XmlNode _configItem { get; set; }
        public static Dictionary<string, string> Parameters { get; set; }

        /// <summary>
        /// Load Parameters from XML configuration and validate them.
        /// </summary>
        /// <param name="configItem">Item XML node</param>
        /// <returns>A list of string with all validations errors found.</returns>
        public static void LoadConfigItem(XmlNode configItem)
        {

            _configItem = configItem;
            if (_configItem == null)
                throw new ArgumentNullException("confgItem", "Configuration Item can't be null.");

            _errors = new List<string>();
            Parameters = new Dictionary<string, string>();
            if (_configItem.HasChildNodes)
            {

                // Param Data.
                string paramName = string.Empty;
                string paramValue = string.Empty;

                // Fill Parameter dictionary
                foreach (XmlNode param in _configItem.ChildNodes)
                {
                    if (param.NodeType != XmlNodeType.Element)
                        continue;
                    paramName = param.Name;
                    paramValue = param.WrapConfigString();
                    if (Parameters.ContainsKey(paramName))
                    {
                        AddError(string.Format("Parameter '{0}' duplicated.", paramName));
                        continue;
                    }
                    Parameters.Add(paramName, paramValue.Trim());
                }

                // Now that we already have the parameters values, lets do the reeplacement of {%paramname%} with param value.                
                List<string> lParamKeys = new List<string>(Parameters.Keys);
                foreach (string paramKey in lParamKeys)
                {
                    paramValue = Parameters[paramKey];
                    if (paramValue.Contains("{%") && paramValue.Contains("%}"))
                    {
                        // TODO: Do it recursive.
                        // Reeplace vars/
                        Parameters[paramKey] = rxParams.Replace(paramValue, m =>
                        {
                            string retval = m.Value;
                            string param2ReplaceKey = m.Value.Substring(2).Substring(0, m.Value.IndexOf("%}") - 2);
                            if (Parameters.ContainsKey(param2ReplaceKey))
                                retval = Parameters[param2ReplaceKey];
                            return retval;
                        });
                    }
                }

                // Do Validations                
                bool requiredAtt = false;
                //bool CreateDirectoryIfNotExist = false;
                foreach (XmlNode param in _configItem.ChildNodes)
                {

                    if (param.NodeType != XmlNodeType.Element)
                        continue;

                    // Validations
                    if (param.Attributes != null && param.Attributes.Count > 0)
                    {

                        // Try to get Atts
                        try { requiredAtt = bool.Parse(param.Attributes["required"].Value); }
                        catch { }
                        //try { CreateDirectoryIfNotExist = bool.Parse(param.Attributes["CreateDirectoryIfNotExist"].Value); }catch { }

                        // Do Validations
                        paramValue = Parameters[param.Name];
                        if (requiredAtt && string.IsNullOrEmpty(paramValue)) AddError(string.Format("Parameter '{0}' is required.", paramName));

                    }
                }

            }

        }

        private static void AddError(string errorMsg)
        {
            if (_errors != null)
                _errors = new List<string>();
            _errors.Add(errorMsg);
        }

        public static string GetConfigString(XmlNode ConfigXmlNode, string parameterName)
        {
            string retval = string.Empty;
            if (string.IsNullOrEmpty(parameterName))
                return retval;
            try
            {
                retval = ConfigXmlNode.SelectSingleNode(parameterName).InnerText.Trim();
            }
            catch { }
            return retval;
        }

        private static string WrapConfigString(this XmlNode ConfigXmlNode)
        {
            string retval = string.Empty;
            try { retval = ConfigXmlNode.InnerText.Trim(); }
            catch { }
            return retval;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameterName"></param>
        /// <returns></returns>
        public static string GetConfigParameter(string parameterName)
        {
            string retval = string.Empty;
            if (string.IsNullOrEmpty(parameterName))
                return retval;
            try
            {
                if (Parameters.ContainsKey(parameterName))
                    retval = Parameters[parameterName];
            }
            catch { }
            return retval;
        }

    }

    #endregion

}
