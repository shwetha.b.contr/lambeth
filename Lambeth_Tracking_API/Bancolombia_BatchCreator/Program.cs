﻿using IMShortProcess;
using Spectre.Console;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace Bancolombia_BatchCreator
{
    class Program
    {
        // Create a table
        static Table table = new Table();
        static string itemName = string.Empty;

        static void Main(string[] args)
        {
            var config = new JobConfigurationItem();
            config.Configuration = MockItemConfig();
            var script = new IMShortProcessScript();
            script.TriggeredInProgress += ConLog;

            itemName = config.Configuration.Attributes.GetNamedItem("name").Value;

            script.Run(config);

            Console.ReadLine();
        }

        private static void ConLog(object sender, string msg, string status, int progress)
        {
            CreateTable();

            // Remove the first one
            table.Rows.RemoveAt(0);

            // Add some rows
            table.AddRow(itemName, msg.Replace('[', ' ').Replace(']', ' '), DateTime.Now.ToShortDateString(), status, progress.ToString());

            AnsiConsole.Write(table);
            Thread.Sleep(500);
            if (progress != 100)
            {
                Console.Clear();
            }
        }

        private static void CreateTable()
        {
            table = new Table() { Border = TableBorder.Rounded }.Expand();

            table.Title = new TableTitle("IMSHORT TESTING");

            // Add some columns
            table.AddColumn(new TableColumn("Job Name").Centered());
            table.AddColumn(new TableColumn("Message").Centered());
            table.AddColumn(new TableColumn("Last Time").Centered());
            table.AddColumn(new TableColumn("Status").Centered());
            table.AddColumn(new TableColumn("Progress").Centered());

            // Add some rows
            table.AddRow(itemName, "", DateTime.Now.ToShortDateString(), "", "");
        }

        private static XmlNode MockItemConfig()
        {
            var xdoc = new XmlDocument();
            xdoc.Load(@"MockConfig.xml");
            return xdoc.SelectSingleNode("config/items/item");
        }
    }
}
