﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMLogger.Entities
{
    public class Config
    {
        public string OutBoundPath { get; set; }
        public string ProjectName { get; set; }

    }
}
