﻿using IMLogger.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace IMLogger
{
    public class Log
    {
        private string path;
        private string datetime;
        private Config c;
        private static Log _instance;

        private Log()
        {
            
            try
            {

                //string xml = Path.Combine(@"Lambeth_Tracking_11032024", @"C:\Projects\2024\Lambeth_Tracking_11032024\Logs.xml");

                //ReadXML(xml);
                //datetime = DateTime.Now.ToString("MM-dd-yyyy");
                //path = String.Format("{0}\\{1}", c.OutBoundPath.Trim(), datetime);

                //if (!Directory.Exists(path))
                //{
                //    Directory.CreateDirectory(path);
                //}
            } catch(Exception ex) {

            }
        }

        public static Log GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Log();
            }
            return _instance;
        }

        public void setMessage(string taskName, string descriptionTask, string userID)
        {
            try
            {
                string pathFile = ("");
                //string pathFile = String.Format("{0}\\{1}_{2}.txt", path, c.ProjectName.Trim(), userID.Trim());
                string line = String.Format("{0} - {1} - {2}", DateTime.Now, taskName.Trim(), descriptionTask.Trim());

                if (!File.Exists(pathFile))
                {
                    System.IO.FileStream f = System.IO.File.Create(pathFile);
                    f.Close();
                }

                using (StreamWriter _StreamWriter = new StreamWriter(pathFile, true))
                {
                    _StreamWriter.WriteLine(line);
                }
            } catch(Exception ex) { }
        }

        private void ReadXML(string xml)
        {
            try
            {
                c = new Config();
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(xml);
                c.ProjectName = xmlDoc.GetElementsByTagName("ProjectName").Item(0).LastChild.Value;
                c.OutBoundPath = xmlDoc.GetElementsByTagName("OutBoundPath").Item(0).LastChild.Value;
            } catch(Exception ex) { }
        }
    }
}
